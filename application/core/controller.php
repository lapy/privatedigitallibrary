<?php

class Controller
{
    public $pendingEdits = null;
    public $mailObj = null;

    //costruttore, carica tutti i modelli e aggiorna i documenti in revisione
    function __construct()
    {
        //load all models
        foreach (glob("../models/*.php") as $filename)
        {
            include $filename;
        }
        $this->updatePendingEdits();
    }

    //php magic getter, se è richiesta una proprietà di classe che non è definita
    //php chiama automaticamente il metodo get_proprietà() se definito
    //ES: $this->mail chiama $this->get_Mail();
    function __get($property) {
        $method = "get_$property";
        if(method_exists($this, $method)) return $this->$method();
        user_error("undefined property $property");
    }

    //funzione che restituisce l'ogetto PHPMailer, istanziandolo solo se necessario
    public function get_Mail()
    {
        if(!isset($this->mailObj))
        {
            $this->mailObj = new PHPMailer;
            $this->mailObj->isSMTP();
            $this->mailObj->Host = SMTP_HOST;
            $this->mailObj->SMTPAuth = SMTP_AUTH;
            $this->mailObj->Username = SMTP_USERNAME;
            $this->mailObj->Password = SMTP_PASSWORD;
            $this->mailObj->SMTPSecure = SMTP_SECURE;
            $this->mailObj->Port = SMTP_PORT;
            $this->mailObj->From = SMTP_FROM;
            $this->mailObj->FromName = SMTP_FROMNAME;   
        }
        return $this->mailObj;
    }

    //funzione che aggiorna il numero di documenti che richiedono la revisione di modifiche
    //relative all'utente corrente
    public function updatePendingEdits()
    {
        if($this->isUserManager()) {
            $this->pendingEdits = Document_modification_token::find('all', array(
                'conditions' => array( 'admin_token IS NOT ?', NULL )
            ));
        }
        else if($this->isUserLogged())
        {
            $this->pendingEdits = Document_modification_token::find('all', array(
                'conditions' => array( 'user_id = ? AND user_token IS NOT ?', $_SESSION['user']->id, NULL )
            ));
        }
    }

    //funzione che controlla se l'utente è loggato
    public function isUserLogged()
    {
    	if(isset($_SESSION['user']))
    		return true;
    	else
    		return false;
    }

    //funzione che controlla se l'utente è manager
    public function isUserManager()
    {
    	if($this->isUserLogged())
    		if($_SESSION['user']->role == 0)
    			return true;
    	return false;
    }

    //funzione che controlla se l'utente è attivato o in fase di attivazione
    //(non si è ancora registrato dopo essere stato inserito da manager)
    public function isUserVerified()
    {
        if($_SESSION['user']->activated == 1)
            return true;
        else
            return false;
    }

    //funzione che crea i link a partire dai valori di controller, azione e parametro.
    public function createLink($controller, $action = null, $parameter = null)
    {
        $link = URL.'/'.$controller.'/';
        if(isset($action))
            $link .= $action.'/';
        if(isset($parameter))
            $link .= $parameter;
        return $link;
    }

    //funzione per generare stringhe random
    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        return $randomString;
    }

    //funzione necessaria alla validazione dei form
    public function ajax ($data)
    {
        if(isset($_POST[$data])) {
            switch ($data) {
                case 'username':
                    if(User::find_by_username($_POST[$data])) {
                        echo json_encode(true);
                    }
                    else {
                        json_encode(false);
                    }
                    break;
                
                case 'email':
                    if(User::find_by_email($_POST[$data])) {
                        echo json_encode(true);
                    }
                    else {
                        echo json_encode(false);
                    }
                    break;

                case 'name':
                    if($user = User::find_by_verification_token($_POST['token'])) {
                        if($user->name == $_POST[$data])
                            echo json_encode(true);
                        else
                            echo json_encode(false);
                    }
                    else {
                        json_encode(false);
                    }
                    break;

                case 'surname':
                    if($user = User::find_by_verification_token($_POST['token'])) {
                        if ($user->surname == $_POST[$data])
                            echo json_encode(true);
                        else
                            echo json_encode(false);
                    }
                    else {
                        echo json_encode(false);
                    }
                    break;

                case 'email_verification':
                    if($user = User::find_by_verification_token($_POST['token'])) {
                        if($user->email == $_POST[$data])
                            echo json_encode(true);
                        else
                            echo json_encode(false);
                    }
                    else {
                        json_encode(false);
                    }
                    break;

                case 'email_edit':
                    if($user = User::find_by_email($_POST[$data])) {
                        if ($user->username == $_SESSION['user']->username)
                            echo json_encode(true);
                        else
                            echo json_encode(false);
                    }
                    else {
                        echo json_encode(true);
                    }
                    break;
            }
        }
    }
}
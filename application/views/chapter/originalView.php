<div id="contents" class="fullwidth">
<div class="comparison">
	<h1 class="title">Originale</h1>
	<div class="document">
		<div class="field title"><div class="label">Titolo: </div><?php echo $originalDocument->title; ?></div>
		<div class="field author"><div class="label">Autore: </div><?php echo $originalDocument->author; ?></div>
		<div class="field year"><div class="label">Anno: </div><?php echo $originalDocument->year; ?></div>
		<div class="field url"><div class="label">Url: </div><?php echo '<a href="'.$originalDocument->url.'">'.$originalDocument->url.'</a>'; ?></div>
		<div class="field status"><div class="label">Visibilità: </div>
			<?php 
				if($originalDocument->status==0)
					echo 'Privata';
				else
					echo 'Pubblica';
			?>
		</div>
		<div class="field taglist"><div class="label">Tag: </div><?php foreach ($originalDocument->tags as $tag) {
			echo '<span class="tag"><a href="' . $this->createLink('document', 'tag', $tag->name) . '">' . $tag->name . '</a></span>';
		} ?></div>
		<div class="field comment"><div class="label">Commenti: </div><?php echo $originalDocument->comment; ?></div>

		<div class="field chapter_title"><div class="label">Tratto da: </div><?php echo $originalDocument->info->name; ?>, casa editrice <?php echo $originalDocument->info->editor; ?></div>
		<div class="field curators"><div class="label">Curatori: </div><?php echo $originalDocument->info->curators; ?></div>
		<div class="field pages"><div class="label">Dimensione: </div>
		<?php if ($originalDocument->info->end_page == $originalDocument->info->start_page) { ?> 1 pagina, pag. <?php echo $originalDocument->info->start_page; }
		else { echo ($originalDocument->info->end_page - $originalDocument->info->start_page + 1); ?> pagine, da pag. <?php echo $originalDocument->info->start_page; ?> a pag. <?php echo $originalDocument->info->end_page; } ?></div>
		<div class="field createdBy"><div class="label">Inserito da: </div><a href="<?php echo $this->createLink('user','profile',$originalDocument->user->username); ?>"><?php echo $originalDocument->user->username; ?></a></div>
	</div>
</div>
<!-- END CONTENTS -->
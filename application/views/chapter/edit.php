<div id="contents">
<!-- CHAPTER EDIT -->
	<h1>Modifica Capitolo</h1>
	<form class="aligned" id="createDocument" method="post" action="<?php echo $this->createLink('chapter','edit', $document->id); ?>">
		<fieldset style="float:left;">
			<legend>Informazioni Generali</legend>
			<div class="general">
				<div class="control-group"><label for="title">Titolo</label>
				<input id="title" name="title" type="text" size="30" maxlength="255" value="<?php if(isset($document->title)) echo $document->title; ?>" /></div>
				
				<div class="control-group"><label for="author">Autore</label>
				<input id="author" name="author" type="text" size="30" maxlength="255" value="<?php if(isset($document->author)) echo $document->author; ?>" /></div>
				
				<div class="control-group"><label for="year">Anno</label>
				<select id="year" name="year"><option value="">Seleziona</option></select></div>
				
				<div class="control-group"><label for="url">URL</label>
				<input id="url" name="url" type="text" size="30" maxlength="255" value="<?php if(isset($document->url)) echo $document->url; ?>" /></div>
					
				<div id="visibility" class="control-group radiogroup"><span class="visibilityLabel">Visibilità</span>
					<input id="status" name="status" type="radio" value="0" <?php if($document->status == 0) echo 'checked="checked"'; ?> /><label class="radio" for="status" >Privata</label>
					<input id="status2" name="status" type="radio" value="1" <?php if($document->status == 0) echo 'checked="checked"'; ?> /><label class="radio" for="status2">Pubblica</label>
				</div>
				
				<div class="control-group"><label for="tags" class="tagLabel">Tag</label>
				<select id="tags" name="tags[]" multiple="multiple" class="tags" style="width: 300px;"><option>Seleziona</option></select></div>
				
				<div class="control-group"><label for="comment">Commenti</label>
				<textarea id="comment" name="comment" cols="27" rows="5"><?php if(isset($document->comment)) echo $document->comment; ?></textarea></div>
			</div>

			<legend>Informazioni di Tipo</legend>
			<div class="info">
			<!-- SPECIFIC INFO -->

				<div class="control-group"><label for="chapter_name">Titolo libro</label>
				<input id="chapter_name" name="chapter_name" type="text" size="30" maxlength="255" value="<?php if(isset($document->info->name)) echo $document->info->name; ?>" />
				</div>

				<div class="control-group"><label for="chapter_editor">Casa editrice</label>
				<input id="chapter_editor" name="chapter_editor" type="text" size="30" maxlength="255" value="<?php if(isset($document->info->editor)) echo $document->info->editor; ?>" />
				</div>

				<div class="control-group"><label for="chapter_curators">Curatori del libro</label>
				<input id="chapter_curators" name="chapter_curators" type="text" size="30" maxlength="255" value="<?php if(isset($document->info->curators)) echo $document->info->curators; ?>" />
				</div>

				<div class="control-group"><label for="chapter_start_page">Pagina iniziale</label>
				<input id="chapter_start_page" name="chapter_start_page" type="text" size="4" maxlength="4" value="<?php if(isset($document->info->start_page)) echo $document->info->start_page; ?>" />
				</div>

				<div class="control-group"><label for="chapter_end_page">Pagina finale</label>
				<input id="chapter_end_page" name="chapter_end_page" type="text" size="4" maxlength="4" value="<?php if(isset($document->info->end_page)) echo $document->info->end_page; ?>" />
				</div>
			</div>

			<div class="controls">
				<input class="button button-primary" type="submit" value ="<?php if($document->user_id != $_SESSION['user']->id) echo 'Proponi Modifica'; else echo 'Modifica'; ?>" />
				<input class="button" type="reset" value="Reset" />
			</div>
		</fieldset>
	</form>
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
//<![CDATA[
$( document ).ready(function() {

	<?php if($document->status == 0) echo 'var status = "#status";'; else echo 'var status = "#status2";'; ?>

	$(status).attr('checked','checked').button("refresh");

	$('#createDocument').bind('keypress keydown keyup', function(e){
	   if(e.keyCode == 13) { e.preventDefault(); }
	});

	$('#tags').tokenize({
	    datas: "<?php echo $this->createLink('tag', 'ajax', 'search'); ?>",
	    searchParam: 'q',
	    beforeAddNewToken:	function(value, text){
	    	var newId=null;
	    	if(value == text) {
	    		//add tag to db
	    		$.ajax({
	    			url: "<?php echo $this->createLink('tag', 'ajax', 'create'); ?>/" + value,
	    			async: false,
	    			success: function(result){
	    				//alert('Tag Inserito!');
	    				newId=JSON.parse(result);
	    			}
	    		});
	    	}
	    	return newId;
	    },
	    onInitAddTokenArray: {<?php foreach ($document->tags as $tag) { echo $tag->id; ?>:'<?php echo $tag->name; ?>',<?php } ?>}
	});
	
	$("#createDocument").validate(  
	{  
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		},
		highlight: function(element, errorClass) {
			$(element).addClass('error');
			$(element).next().removeClass("checked");
		},
	    rules:  
	    {  
    	'title':{
    		required: true,
    		maxlength: 100
			},
		'author':{
			required: true,
    		minlength: 2,
    		maxlength: 100
			},
		'year':{
			required: true,
			},
		'url':{
			required: true,
			url: true,
			maxlength: 255
			},
		'type':{
			required: true
			},
		'tags':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'comment':{
			minlength: 3,
    		maxlength: 255
			},
		'chapter_name':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'chapter_editor':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'chapter_curators':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'chapter_start_page':{
			required: true,
			digits: true
			},
		'chapter_end_page':{
			required: true,
			digits: true,
			lessThanEqual: 'chapter_start_page'
			}
	    },
	    messages:  
	    {  
	    'title':{
	    	required: 'Il campo titolo è obbligatorio.',
	    	maxlength: 'Inserisci un titolo di massimo 3 caratteri.'
			},
		'author':{
			required: 'Il campo autore è obbligatorio.',
	    	minlength: 'Inserisci un autore di almeno 2 caratteri.'
			},
		'year':{
			required: 'Il campo anno è obbligatorio',
			},
		'url':{
			required: 'Il campo url è obbligatorio.',
			url: 'Inserisci un url valido.',
			maxlength: 'Inserisci un url di massimo 255 caratteri.'
			},
		'comment':{
			minlength: 'Inserisci un commento di almeno 3 caratteri.',
    		maxlength: 'Inserisci un commento di massimo 255 caratteri.'
			},
		'chapter_name':{
			required: 'Il campo titolo libro è obbligatorio.',
			minlength: 'Inserisci un titolo libro di almeno 3 caratteri.',
    		maxlength: 'Inserisci un titolo libro di massimo 100 caratteri.'
			},
		'chapter_editor':{
			required: 'Il campo casa editrice libro è obbligatorio.',
			minlength: 'Inserisci una casa editrice di almeno 3 caratteri.',
    		maxlength: 'Inserisci una casa editrice di massimo 100 caratteri.'
			},
		'chapter_curators':{
			required: 'Il campo curatori del libro libro è obbligatorio.',
			minlength: 'Inserisci curatori del libro di almeno 3 caratteri.',
    		maxlength: 'Inserisci curatori del libro di massimo 100 caratteri.'
			},
		'chapter_start_page':{
			required: 'Il campo pagina iniziale è obbligatorio.',
			digits: 'Il campo pagina iniziale deve essere composta da sole cifre'
			},
		'chapter_end_page':{
			required: 'Il campo pagina finale è obbligatorio.',
			digits: 'Il campo pagina finale deve essere composta da sole cifre.',
			lessThanEqual: 'Il campo pagina finale deve essere maggiore o uguale a quello di pagina iniziale.'
			}
	    }  
	});

	yearSelect(<?php if(isset($document->year)) echo $document->year; ?>);
});
//]]>
</script>
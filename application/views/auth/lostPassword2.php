<div id="contents">
	<h1 class="title">Password Smarrita</h1>
	<form id="lost_password" class="aligned" method="post" action="<?php echo $this->createLink('auth','lostPassword', $token); ?>">
		<fieldset>
			<legend>Inserisci la tua nuova password</legend>
		 	<div class="control-group"><label for="password-lost">Password</label>
		 		<input id="password-lost" name="password-lost" type="password" size="30" maxlength="255" />
		 	</div>

		 	<div class="control-group"><label for="okpassword">Conferma Password</label>
		 		<input id="okpassword" name="okpassword" type="password" size="30" maxlength="255" disabled="disabled" />
		 	</div>

		 	<div class="controls">
		 		<input class="button button-primary" type="submit" value ="Invia dati" />
				<input class="button" type="reset" value="Annulla" />
			</div>
		</fieldset>
	</form>
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
//<![CDATA[
$( document ).ready(function() {

	// funzione che abilita/disabilita il campo conferma password
	$('#password-lost').keyup(function() {
		if ($('#password-lost').val() != "") {
			$('#okpassword').removeAttr("disabled");
		}
		else {
			$('#okpassword').attr("disabled", "disabled");
		}
	});

	$("#lost_password").validate(  
	{
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		},
		highlight: function(element, errorClass) {
			$(element).addClass('error');
			$(element).next().removeClass("checked");
		},  
	    rules:  
	    {
    	'password-lost':{  
        	required: true,
        	minlength: 4
        },
	    'okpassword':{  
	        required: true,
	        equalTo: '#password-lost'
	        }
	    },
	    messages:  
	    {
		    'password-lost':{  
	        	required: 'Il campo password è obbligatorio.',
	        	minlength: 'Inserisci una password di almeno 4 caratteri.',
	        	},
		    'okpassword':{  
		        required: 'Il campo conferma password è obbligatorio.',
		        equalTo: 'Le due password inserite non sono corrispondenti.'
		        }
	    }  
	});
});
//]]>
</script>
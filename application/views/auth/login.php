<div id="contents" class="fullwidth">
	<h1 class="title">Login</h1>
	<form class="aligned" action="<?php echo $this->createLink('auth','login'); ?>" method="post">
		<div class="control-group"><label for="username-form">Username</label>
		<input id="username-form" name="username" type="text" /></div>

		<div class="control-group"><label for="password-form">Password</label>
		<input id="password-form" name="password" type="password" /></div>

		<p>
			<input name="redirect" type="hidden" value="<?php echo URL; ?>" />
		</p>
			
		<div class="controls">
			<p>
				<input class="button button-primary" type="submit" value="Login" />
				<span>
					<a href="<?php echo $this->createLink('auth', 'lostPassword'); ?>">Password smarrita?</a>
		        </span>
		    </p>
		</div>
	</form>
</div>
<!-- END CONTENTS -->
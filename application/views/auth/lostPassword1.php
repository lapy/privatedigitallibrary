<div id="contents">
	<h1 class="title">Password Smarrita</h1>
	<form id="lost_password" class="aligned" method="post" action="<?php echo $this->createLink('auth','lostPassword'); ?>" id="lost1" >
		<fieldset>
		<legend>Inserisci il tuo username e la tua email per continuare</legend>
		 	<div class="control-group"><label for="username-lost">Username</label>
		 		<input id="username-lost" name="username" type="text" size="30" maxlength="255" />
		 	</div>

		 	<div class="control-group"><label for="email-lost">eMail</label>
		 		<input id="email-lost" name="email" type="text" size="30" maxlength="255" />
		 	</div>
		 	
		 	<div class="controls">
		 		<input class="button button-primary" type="submit" value ="Invia dati" />
				<input class="button" type="reset" value="Annulla" />
			</div>
		</fieldset>
	</form>
</div>
<script type="text/javascript">
//<![CDATA[
$( document ).ready(function() {
	
	$("#lost_password").validate(  
	{
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		},
		highlight: function(element, errorClass) {
			$(element).addClass('error');
			$(element).next().removeClass("checked");
		},  
	    rules:  
	    {
    	'username':{  
        	required: true
        },
	    'email':{  
	        required: true,  
	        email: true
	        }
	    },
	    messages:  
	    {
	    'username':{  
        	required: "Il campo username è obbligatorio."
        },
	    'email':{  
	        required: "Il campo email è obbligatorio.",  
	        email: "Inserisci un indirizzo email valido.",
	        }
	    }  
	});
});
//]]>
</script>
<!-- END CONTENTS -->
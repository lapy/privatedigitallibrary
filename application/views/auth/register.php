<div id="contents">
	<h1 class="title">Registrazione Utente</h1>
	<form class="aligned" method="post" action="<?php echo $this->createLink('auth','register', $token); ?>" id="userregister">
		<fieldset>
			<legend>Compila i campi correttamente per registrarti</legend>
		 	<div class="control-group"><label for="name">Nome</label>
		 		<input id="name" name="name" type="text" size="30" maxlength="255" />
		 	</div>
		 	<div class="control-group"><label for="surname">Cognome</label>
		 		<input id="surname" name="surname" type="text" size="30" maxlength="255" />
		 	</div>
		 	<div class="control-group"><label for="email_verification">eMail</label>
		 		<input id="email_verification" name="email_verification" type="text" size="30" maxlength="255" />
		 	</div>
		 	<div class="control-group"><label for="username-register">Username</label>
		 		<input id="username-register" name="username" type="text" size="30" maxlength="255" />
		 	</div>
		 	<div class="control-group"><label for="password-register">Password</label>
		 		<input id="password-register" name="password" type="password" size="30" maxlength="255" />
		 	</div>
		 	<div class="control-group"><label for="okpassword">Conferma Password</label>
		 		<input id="okpassword" name="okpassword" type="password" size="30" maxlength="255" disabled="disabled" />
		 	</div>

	 		<div class="controls">
	 			<input class="button button-primary" type="submit" value ="Invia dati" />
				<input class="button" type="reset" value="Reset" />
			</div>
		</fieldset>
	</form>
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
var verification_token='<?php if (isset($token)) echo $token; ?>';

$( document ).ready(function() {

	// funzione che abilita/disabilita il campo conferma password
	$('#password-register').keyup(function() {
		if ($('#password-register').val() != "") {
			$('#okpassword').removeAttr("disabled");
		}
		else {
			$('#okpassword').attr("disabled", "disabled");
		}
	});
	
	$("#userregister").validate(  
	{
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		},
		highlight: function(element, errorClass) {
			$(element).addClass('error');
			$(element).next().removeClass("checked");
		},  
	    rules:  
	    {  
	    'name':{  
	        required: true,
	        validateData: true 
	        }, 
	    'surname':{  
	        required: true,
	        validateData: true 
	        },
	   	'email_verification':{  
	        required: true,  
	        email: true,
	        validateData: true
	        },
	    'username':{  
	        required: true,  
	        minlength: 3,
	        checkInsideDB: true
	        },
	   	'password':{  
	        required: true,  
	        minlength: 4  
	        },
	    'okpassword':{  
	        required: true,  
	        equalTo: '#password' 
	        }
	    },  
	    messages:  
	    {  
	    'name':{  
	        required: "Il campo nome è obbligatorio.",
	        validateData: "Il nome inserito non corrisponde a quello presente nel database." 
	        },
	    'surname':{  
	        required: "Il campo cognome è obbligatorio.",
	        validateData: "Il cognome inserito non corrisponde a quello presente nel database." 
	        },  
	    'email_verification':{  
	        required: "Il campo email è obbligatorio.",  
	        email: "Inserisci un indirizzo email valido.",
	        validateData: "L'indirizzo email inserito non corrisponde a quello presente nel database. Potrai cambiarlo in un secondo momento."  
	        },
	    'username':{
	    	required: "Il campo username è obbligatorio.",
	    	minlength: "Inserisci un username di almeno 3 caratteri.",
	    	checkInsideDB: "L'username inserito non è disponibile."
	    	},
	    'password':{
	    	required: "Il campo password è obbligatorio.",
	    	minlength: "Inserisci una password di almeno 4 caratteri."
	    	},
	    'okpassword':{
	    	required: "Il campo conferma password è obbligatorio.",
	    	equalTo: "Le due password inserite non sono corrispondenti."
	    	},
	    }  
	});
});
</script>
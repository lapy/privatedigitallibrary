<div id="contents">
	<h1>Invita utente</h1>
	<form id="userinsert" class="aligned" method="post" action="<?php echo $this->createLink('user','insert'); ?>">
	 	 
	 	<div class="control-group"><label for="name">Nome</label>
	 		<input id="name" name="name" type="text" size="30" maxlength="255" /></div>
	 	
	 	<div class="control-group"><label for="surname">Cognome</label>
	 		<input id="surname" name="surname" type="text" size="30" maxlength="255" /></div>
	 	
	 	<div class="control-group"><label for="email">eMail</label>
	 		<input id="email" name="email" type="text" size="30" maxlength="255" /></div>
	 	
	 	<div class="controls">
	 		<input class="button button-primary" type="submit" value="Invia dati" />
			<input class="button" type="reset" value="Annulla" />
		</div>
 	</form>
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
//<![CDATA[
$( document ).ready(function() {
	$("#userinsert").validate(  
	{  
	    rules:  
	    {  
	    'name':{  
	        required: true,  
	        minlength: 2  
	        }, 
	    'surname':{  
	        required: true,  
	        minlength: 2  
	        },  
	    'email':{  
	        required: true,  
	        email: true,
	        checkInsideDB: true
	        }
	    },  
	    messages:  
	    {  
	    'name':{  
	        required: "Il campo nome è obbligatorio!",  
	        minlength: "Scegli un nome di almeno 2 lettere!"  
	        },
	    'surname':{  
	        required: "Il campo cognome è obbligatorio!",  
	        minlength: "Scegli un cognome di almeno 2 lettere!"  
	        },  
	    'email':{  
	        required: "Il campo email è obbligatorio!",  
	        email: "Inserisci un valido indirizzo email!",
	        checkInsideDB: "L'indirizzo email inserito è già presente nel database"  
	        }  
	    }  
	});
});
//]]>
</script>

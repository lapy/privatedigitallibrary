<div id="contents">
	<h1 class="title">Modifica profilo</h1>
	<form class="aligned" method="post" action="<?php echo $this->createLink('user','edit'); ?>" id="useredit" >
		<fieldset>
			<legend>Dati profilo</legend>
		 	<div class="control-group"><label for="name">Nome</label>
		 		<input id="name" name="name" type="text" size="30" maxlength="255" value="<?php echo $_SESSION['user']->name ?>" disabled="disabled" />
		 	</div>
		 	<div class="control-group"><label for="surname">Cognome</label>
		 		<input id="surname" name="surname" type="text" size="30" maxlength="255" value="<?php echo $_SESSION['user']->surname ?>" disabled="disabled" />
		 	</div>
		 	<div class="control-group"><label for="username">Username</label>
		 		<input id="username" name="username" type="text" size="30" maxlength="255" value="<?php echo $_SESSION['user']->username ?>" disabled="disabled" />
		 	</div>
		 	<div class="control-group"><label for="email_edit">eMail</label>
		 		<input id="email_edit" name="email_edit" type="text" size="30" maxlength="255" value="<?php echo $_SESSION['user']->email ?>" />
		 	</div>
		 	<div class="control-group"><label for="password">Password Attuale</label>
		 		<input id="password" name="password" type="password" size="30" maxlength="255"/>
		 	</div>
		 	<div class="control-group"><label for="newpassword">Nuova password</label>
		 		<input id="newpassword" name="newpassword" type="password" size="30" maxlength="255"/>
		 	</div>
		 	<div class="control-group"><label for="okpassword">Conferma password</label>
		 		<input id="okpassword" name="okpassword" type="password" size="30" maxlength="255" disabled="disabled" />
		 	</div>

		 	<div class="controls">
		 		<input class="button button-primary" type="submit" value ="Invia dati" />
				<input class="button" type="button" value="Annulla" onclick="window.location='<?php echo $this->createLink('user','profile'); ?>'" />
			</div>
		</fieldset>
	</form>
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
//<![CDATA[
//funzione che abilita/disabilita il campo conferma password
$('[name="newpassword"]').keyup(function() {
	if ($('[name="newpassword"]').val() != "") {
		$('[name="okpassword"]').removeAttr("disabled");
	}
	else {
		$('[name="okpassword"]').attr("disabled", "disabled");
	}
});

//validazione form tramite jquery validate
$( document ).ready(function() {
	$("#useredit").validate(  
	{  
	    success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		},
		highlight: function(element, errorClass) {
			$(element).addClass('error');
			$(element).next().removeClass("checked");
		},
		rules:  
	    {  
	    'email_edit':{
	        email: true,
	        checkEmail: true
	    	},
	    'password':{  
	        required: true
	        }, 
	    'newpassword':{  
	        minlength: 4  
	        },
	    'okpassword':{
	    	minlength: 4,
	    	equalTo: '#newpassword'
	        }
	    },  
	    messages:  
	    {  
	    'email_edit':{
	        email: "Inserisci un indirizzo email valido.",
	        checkEmail: "L'indirizzo email inserito è già presente nel database"  
	        },
	    'password':{  
	        required: "Il campo password attuale è obbligatorio"
	        },
	    'newpassword':{
	        minlength: "Inserisci una password di almeno 4 caratteri."  
	        },
	    'okpassword':{
	     	minlength: "Inserisci una password di almeno 4 caratteri.",
	     	equalTo: "Le due password inserite non sono corrispondenti."
	    	}  
	    }
	});
});
//]]>
</script>
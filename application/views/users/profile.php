<div id="contents" class="fullwidth">
<?php if($this->isUserManager() || ( $this->isUserLogged() && $_SESSION['user']->username == $user->username)) { ?>
 	<h1 class="title">Profilo di <?php echo $user->username ?></h1>
 	<p>Nome: <span style="font-style: italic;"><?php echo $user->name ?></span></p>
 	<p>Cognome: <span style="font-style: italic;"><?php echo $user->surname ?></span></p>
 	<p>E-mail: <span style="font-style: italic;"><?php echo $user->email ?></span></p>

<?php } if($this->isUserLogged() && $_SESSION['user']->username == $username) { ?>
	<button type = "button" onclick="window.location='<?php echo URL . '/user/edit' ?>'"> Modifica credenziali </button>
<?php } ?>
	<h1 class="title">Schede inserite da <?php echo $user->username ?></h1>

	<div class="controls">
        <p>
            <label>Mostra: <select title="elementi per pagina" class="elementsPerPage">
                <option value="5">5 elementi</option>
                <option value="10">10 elementi</option>
            </select></label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>Cerca: <input title="cerca" class="search" name="filter" type="text"/></label>
            <input class="button button-primary" type="submit" value="INVIO" />
            <input class="button" type="reset" value="Reset"/>
        </p>
    </div>
		
	<div class="documentTable" >
		<table id="documentsTable">
		    <tr class="tableHeader" id="tableHeader">
		        <td>Titolo</td>
				<td>Autore</td>
				<td>Anno</td>
				<td>Tipo</td>
				<td>Inserito il</td>
				<td>Visibilità</td>
			</tr>
		</table>
	</div>
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
//<![CDATA[
//dati di paginazione
var jsonURL = '<?php echo $this->createLink('document','ajax'); ?>';
var tableID = '#documentsTable';
var userId = '<?php echo $user->id; ?>';
//]]>
</script>
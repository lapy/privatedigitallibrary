<div id="contents" class="fullwidth">
    <h1 class="title">Utenti Registrati</h1>
    <div class="controls">
        <p>
            <label>Mostra: 
                <select title="elementi per pagina" class="elementsPerPage">
                    <option value="5">5 utenti</option>
                    <option value="10">10 utenti</option>
                </select>
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>Seleziona:
                <select title="tipo di utenti" class="userType">
                    <option value="0">Tutti</option>
                    <option value="1">Registrati</option>
                    <option value="2">In attesa</option>
                </select>
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>Cerca: <input title="cerca" class="search" name="filter" type="text" /></label>
            <input class="button button-primary" type="submit" value="INVIO" />
            <input class="button" type="reset" value="Reset"/>
        </p>
    </div>
    <div class="documentTable" >
    	<table id="userTable">
            <tr class="tableHeader" id="tableHeader">
                <td>Username</td>
        		<td>E-mail</td>
        		<td>Registrato dal</td>
        		<td>Ultima modifica</td>
        		<td>Rimuovi</td>
        	</tr>
        </table>
    </div>
</div>
<!-- END CONTENTS -->
<div id="dialog"></div>
<script type="text/javascript">
//<![CDATA[
//dati di paginazione
var jsonURL = '<?php echo $this->createLink('user','userAjax'); ?>';
var tableID = '#userTable';

$(document).ready(function() {

    $("#dialog").dialog({
        modal: true,
        bgiframe: true,
        width: 310,
        height: 180,
        autoOpen: false,
        title: 'Eliminazione Utente'
    });
});
//]]>
</script>
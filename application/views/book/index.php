<div id="contents" class="fullwidth">
    <h1 class="title">Libri</h1>
    <div class="controls">
        <p>
            <label>Mostra: <select title="elementi per pagina" class="elementsPerPage">
                <option value="5">5 elementi</option>
                <option value="10">10 elementi</option>
            </select></label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>Cerca: <input title="cerca" class="search" name="filter" type="text"/></label>
            <input class="button button-primary" type="submit" value="INVIO" />
            <input class="button" type="reset" value="Reset"/>
        </p>
    </div>
    <div class="documentTable">
    	<table id="booksTable">
            <tr class="tableHeader" id="tableHeader">
                <td>Titolo</td>
        		<td>Autore</td>
        		<td>Anno</td>
                <td>Edizione</td>
        		<td>Editore</td>
        		<td>Inserito il</td>
        		<td>Visibilità</td>
        	</tr>
        </table>
    </div> 
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
//<![CDATA[
//dati di paginazione
var jsonURL = '<?php echo $this->createLink('book','ajax'); ?>';
var tableID = '#booksTable';
//]]>
</script>
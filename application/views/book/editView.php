<div class="comparison">
	<h1 class="title">Modificato</h1>
	<div class="document">
		<div class="field title"><div class="label">Titolo: </div><?php echo $document->title; ?></div>
		<div class="field author"><div class="label">Autore: </div><?php echo $document->author; ?></div>
		<div class="field year"><div class="label">Anno: </div><?php echo $document->year; ?></div>
		<div class="field url"><div class="label">Url: </div><?php echo '<a href="'.$document->url.'">'.$document->url.'</a>'; ?></div>
		<div class="field status"><div class="label">Visibilità: </div>
			<?php 
				if($document->status==0)
					echo 'Privato';
				else
					echo 'Pubblico';
			?>
		</div>
		<div class="field taglist"><div class="label">Tag: </div><?php foreach ($document->tags as $tag) {
			echo '<span class="tag"><a href="' . $this->createLink('document', 'tag', $tag->name) . '">' . $tag->name . '</a></span>';
		} ?></div>
		<div class="field comment"><div class="label">Commenti: </div><?php echo $document->comment; ?></div>
		<div class="field book_editor"><div class="label">Casa Editrice: </div><?php echo $document->info->editor; ?></div>
		<div class="field edition"><div class="label">Edizione: </div><?php echo $document->info->edition; ?></div>
	</div>
</div>
</div>
<!-- END CONTENTS -->
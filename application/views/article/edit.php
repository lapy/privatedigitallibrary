<div id="contents">
<!-- ARTICLE EDIT -->
	<h1 class="title">Modifica Articolo</h1>
	<form class="aligned" id="createDocument" method="post" action="<?php echo $this->createLink('article','edit', $document->id); ?>">
		<fieldset style="float:left;">
			<legend>Informazioni Generali</legend>
			<div class="general">
				<div class="control-group"><label for="title">Titolo</label>
				<input id="title" name="title" type="text" size="30" maxlength="255" value="<?php if(isset($document->title)) echo $document->title; ?>" /></div>
				
				<div class="control-group"><label for="author">Autore</label>
				<input id="author" name="author" type="text" size="30" maxlength="255" value="<?php if(isset($document->author)) echo $document->author; ?>" /></div>
				
				<div class="control-group"><label for="year">Anno</label>
				<select id="year" name="year"><option value="">Seleziona</option></select></div>
				
				<div class="control-group"><label for="url">URL</label>
				<input id="url" name="url" type="text" size="30" maxlength="255" value="<?php if(isset($document->url)) echo $document->url; ?>" /></div>
					
				<div id="visibility" class="control-group radiogroup"><span class="visibilityLabel">Visibilità</span>
					<input id="status" name="status" type="radio" value="0" <?php if($document->status == 0) echo 'checked="checked"'; ?> /><label class="radio" for="status" >Privata</label>
					<input id="status2" name="status" type="radio" value="1" <?php if($document->status == 0) echo 'checked="checked"'; ?> /><label class="radio" for="status2">Pubblica</label>
				</div>
				
				<div class="control-group"><label for="tags" class="tagLabel">Tag</label>
				<select id="tags" name="tags[]" multiple="multiple" class="tags" style="width: 300px;"><option>Seleziona</option></select></div>
				
				<div class="control-group"><label for="comment">Commenti</label>
				<textarea id="comment" name="comment" cols="27" rows="5"><?php if(isset($document->comment)) echo $document->comment; ?></textarea></div>
			</div>

			<legend>Informazioni di Tipo</legend>
			<div class="info">
			<!-- SPECIFIC INFO -->
				
				<div class="control-group"><label for="article_name">Titolo rivista</label>
				<input id="article_name" name="article_name" type="text" size="30" maxlength="255" value="<?php if(isset($document->info->name)) echo $document->info->name; ?>" />
				</div>

				<div class="control-group"><label for="article_volume">Volume</label>
				<input id="article_volume" name="article_volume" type="text" size="3" maxlength="3" value="<?php if(isset($document->info->volume)) echo $document->info->volume; ?>" />
				</div>

				<div class="control-group"><label for="article_issue">Numero Uscita</label>
				<input id="article_issue" name="article_issue" type="text" size="3" maxlength="3" value="<?php if(isset($document->info->issue)) echo $document->info->issue; ?>" />
				</div>

				<div class="control-group"><label for="article_start_page">Pagina iniziale</label>
				<input id="article_start_page" name="article_start_page" type="text" size="4" maxlength="4" value="<?php if(isset($document->info->start_page)) echo $document->info->start_page; ?>" />
				</div>

				<div class="control-group"><label for="article_end_page">Pagina finale</label>
				<input id="article_end_page" name="article_end_page" type="text" size="4" maxlength="4" value="<?php if(isset($document->info->end_page)) echo $document->info->end_page; ?>" />
				</div>
			</div>

			<div class="controls">
				<input class="button button-primary" type="submit" value ="<?php if($document->user_id != $_SESSION['user']->id) echo 'Proponi Modifica'; else echo 'Modifica'; ?>" />
				<input class="button" type="reset" value="Reset" />
			</div>
	</fieldset>
	</form>
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
//<![CDATA[
$( document ).ready(function() {

	<?php if($document->status == 0) echo 'var status = "#status";'; else echo 'var status = "#status2";'; ?>

	$(status).attr('checked','checked').button("refresh");

	$('#createDocument').bind('keypress keydown keyup', function(e){
	   if(e.keyCode == 13) { e.preventDefault(); }
	});

	$('#tags').tokenize({
	    datas: "<?php echo $this->createLink('tag', 'ajax', 'search'); ?>",
	    searchParam: 'q',
	    beforeAddNewToken:	function(value, text){
	    	var newId=null;
	    	if(value == text) {
	    		//add tag to db
	    		$.ajax({
	    			url: "<?php echo $this->createLink('tag', 'ajax', 'create'); ?>/" + value,
	    			async: false,
	    			success: function(result){
	    				//alert('Tag Inserito!');
	    				newId=JSON.parse(result);
	    			}
	    		});
	    	}
	    	return newId;
	    },
	    onInitAddTokenArray: {<?php foreach ($document->tags as $tag) { echo $tag->id; ?>:'<?php echo $tag->name; ?>',<?php } ?>}
	});

	$("#createDocument").validate(  
	{  
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		},
		highlight: function(element, errorClass) {
			$(element).addClass('error');
			$(element).next().removeClass("checked");
		},
	    rules:  
	    {  
    	'title':{
    		required: true,
    		maxlength: 100
			},
		'author':{
			required: true,
    		minlength: 2,
    		maxlength: 100
			},
		'year':{
			required: true,
			},
		'url':{
			required: true,
			url: true,
			maxlength: 255
			},
		'comment':{
			minlength: 3,
    		maxlength: 255
			},
		'article_name':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'article_volume':{
			digits: true,
			maxlength: 3
			},
		'article_issue':{
			digits: true,
			maxlength: 3
			},
		'article_start_page':{
			required: true,
			digits: true
			},
		'article_end_page':{
			required: true,
			digits: true,
			lessThanEqual: 'article_start_page'
			}
	    },
	    messages:  
	    {  
	    'title':{
	    	required: 'Il campo titolo è obbligatorio.',
	    	maxlength: 'Inserisci un titolo di massimo 3 caratteri.'
			},
		'author':{
			required: 'Il campo autore è obbligatorio.',
	    	minlength: 'Inserisci un autore di almeno 2 caratteri.'
			},
		'year':{
			required: 'Il campo anno è obbligatorio',
			},
		'url':{
			required: 'Il campo url è obbligatorio.',
			url: 'Inserisci un url valido.',
			maxlength: 'Inserisci un url di massimo 255 caratteri.'
			},
		'comment':{
			minlength: 'Inserisci un commento di almeno 3 caratteri.',
    		maxlength: 'Inserisci un commento di massimo 255 caratteri.'
			},
		'article_name':{
			required: 'Il campo titolo rivista è obbligatorio',
			minlength: 'Inserisci un titolo rivista di almeno 3 caratteri.',
    		maxlength: 'Inserisci un titolo rivista di massimo 3 caratteri.'
			},
		'article_volume':{
			digits: 'Il campo volume deve essere composto da sole cifre.',
			maxlength: 'Inserisci un volume di massimo 3 caratteri.'
			},
		'article_issue':{
			digits: 'Il campo edizione deve essere composto da sole cifre.',
			maxlength: 'Inserisci un\'edizione di massimo 3 caratteri.'
			},
		'article_start_page':{
			required: 'Il campo pagina iniziale è obbligatorio.',
			digits: 'Il campo pagina iniziale deve essere composta da sole cifre.'
			},
		'article_end_page':{
			required: 'Il campo pagina finale è obbligatorio.',
			digits: 'Il campo pagina finale deve essere composta da sole cifre.',
			lessThanEqual: 'Il campo pagina finale deve essere maggiore o uguale a quello di pagina iniziale.'
			},
	    }
	});

	yearSelect(<?php if(isset($document->year)) echo $document->year; ?>);
});
//]]>
</script>
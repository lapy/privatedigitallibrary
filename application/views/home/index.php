<div id="contents" class="homecontents">
<h1 class="title hometitle">Home Page</h1>
	<div id="textcontainer">
		<p>Private Digital Library è un’applicazione web che consente ai membri di un gruppo di ricerca la condivisione di articoli 
		scientifici in ambito informatico scaricati dal web.</p>
		<p>Per fruire di questa applicazione sono necessarie una connessione internet e un dispositivo che supporti la navigazione via browser. 
		Una volta avviata la navigazione sarà possibile l’esplorazione dell’applicazione in modalità anonima con il solo accesso ai
		 documenti con visibilità pubblica.</p>
		<p>Per avere pieno accesso alle funzionalità del sistema bisogna essere ammessi. L’utente responsabile ha facoltà di inserire
		 nel gruppo di ricerca nuovi utenti ammessi, il sistema così invierà un’email all’utente che avrà così facoltà di registrarsi.</p>
		<p>L’utente ammesso è in grado di accedere ai documenti privati, di inserire nuovi documenti e proporre modifiche a documenti
		 esistenti nel caso in cui ravveda errori o imprecisioni; le modifiche proposte saranno notificate sia all’utente inseritore 
		 che al responsabile e saranno effettive solo in caso di duplice accettazione.</p>
		<p>L’utente responsabile oltre ad aver accesso alle funzioni di utente ammesso ha accesso alle funzioni di gestione dell’utenza 
		quali lista utenti, dati utente e rimozione utente.</p>
		<p>La navigazione dei documenti può avvenire per categoria, per tag o per utente inseritore, inoltre può essere ristretta mediante
		 l’utilizzo di parole chiave.</p>
	</div>
	
</div>
<!-- END CONTENTS -->
<div class="sidebar">
	<h1 class="title">Ultimi documenti</h1>
<!--
	    <div class="documentTable homeWidget" >
			<h2>Ultimi documenti inseriti </h2>
	    
			<table class="widgetTable" id="documentsTable">
			    <tr class="tableHeader" id="tableHeader">
			        <td style="width:70%;">Titolo</td>
					<td style="width:30%;">Inserito il</td>
				</tr>
			</table>
		</div>
		-->
		<div class="documentTable homeWidget" >
			<h2>Articoli di Riviste</h2>
	    
			<table class="widgetTable" id="articleTable">
			    <tr class="tableHeader">
			        <td style="width:75%;">Titolo</td>
					<td style="width:25%;">Inserito il</td>
				</tr>
			</table>
		</div>

		<div class="documentTable homeWidget" >
			<h2>Atti di Conferenze</h2>
	    
			<table class="widgetTable" id="actsTable">
			    <tr class="tableHeader">
			        <td style="width:75%;">Titolo</td>
					<td style="width:25%;">Inserito il</td>
				</tr>
			</table>
		</div>

		<div class="documentTable homeWidget" >
			<h2>Capitoli di Libri</h2>
	    
			<table class="widgetTable" id="chapterTable">
			    <tr class="tableHeader">
			        <td style="width:37.5%;">Titolo</td>
			        <td style="width:37.5%;">Tratto da</td>
					<td style="width:25%;">Inserito il</td>
				</tr>
			</table>
		</div>

		<div class="documentTable homeWidget" >
			<h2>Libri</h2>
	    
			<table class="widgetTable" id="booksTable">
			    <tr class="tableHeader">
			        <td style="width:75%;">Titolo</td>
					<td style="width:25%;">Inserito il</td>
				</tr>
			</table>
		</div>
	</div>
<script type="text/javascript">
//<![CDATA[
//dati di paginazione
var jsonURL = '<?php echo $this->createLink('home','ajax'); ?>';
var tableID = '#documentsTable';
fillHomeLatestTable();
//]]>
</script>
<div id="contents" class="fullwidth">
    <h1 class="title">Documenti taggati con "<?php echo $tagName; ?>"</h1>
    <div class="controls">
        <p>
            <label>Mostra: <select title="elementi per pagina" class="elementsPerPage">
                <option value="5">5 elementi</option>
                <option value="10">10 elementi</option>
            </select></label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label><input title="search" class="search" name="filter" type="hidden"/></label>
            <input class="button button-primary" type="hidden" value="INVIO" />
            <input class="button" type="hidden" value="Reset"/>
        </p>
    </div>
    <div class="documentTable" >
        <table id="documentsTable">
            <tr class="tableHeader" id="tableHeader">
                <td>Titolo</td>
				<td>Autore</td>
				<td>Anno</td>
				<td>Tipo</td>
				<td>Inserito il</td>
				<td>Visibilità</td>
            </tr>
        </table>
    </div>    
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
//<![CDATA[
//dati di paginazione
var jsonURL = '<?php echo $this->createLink('document','tagAjax'); ?>';
var tagName = '<?php echo $tagName; ?>';
var tableID = '#documentsTable';
//]]>
</script>
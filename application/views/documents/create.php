<div id="contents">
	<h1 class="title">Nuovo Documento</h1>
	<form id="createDocument" class="aligned" method="post" action="<?php echo $this->createLink('document','create'); ?>">
		<fieldset style="float:left;">
			<legend>Informazioni Generali</legend>
			<div class="general">
				<div class="control-group"><label for="title">Titolo</label>
				<input id="title" name="title" type="text" size="30" maxlength="255" value="<?php if(isset($_POST['title'])) echo $_POST['title']; ?>" /></div>
				
				<div class="control-group"><label for="author">Autore</label>
				<input id="author" name="author" type="text" size="30" maxlength="255" value="<?php if(isset($_POST['author'])) echo $_POST['author']; ?>" /></div>
				
				<div class="control-group"><label for="year">Anno</label>
				<select id="year" name="year"><option value="">Seleziona</option></select></div>
				
				<div class="control-group"><label for="url">URL</label>
				<input id="url" name="url" type="text" size="30" maxlength="255" value="<?php if(isset($_POST['url'])) echo $_POST['url']; ?>" /></div>
					
				<div id="visibility" class="control-group radiogroup"><span class="visibilityLabel">Visibilità</span>
					<input id="status" name="status" type="radio" value="0" checked="checked" /><label class="radio" for="status">Privata</label>
					<input id="status2" name="status" type="radio" value="1" /><label class="radio" for="status2">Pubblica</label>
				</div>
				
				<div class="control-group"><label for="tags" class="tagLabel">Tag</label>
				<select id="tags" name="tags[]" multiple="multiple" class="tags" style="width: 300px;"><option>Seleziona</option></select></div>
				
				<div class="control-group"><label for="comment">Commenti</label>
				<textarea id="comment" name="comment" cols="27" rows="5"></textarea></div>
			</div>
			
			<legend>Informazioni di Tipo</legend>
			<div class="info">
				<div class="control-group radiogroup" ><span class="typeLabel">Tipo di documento</span>
					<input id="type1" name="type" type="radio" value="article" checked="checked" /><label class="radio" for="type1">Articolo</label>
					<input id="type2" name="type" type="radio" value="act" /><label class="radio" for="type2">Atto</label>
					<input id="type3" name="type" type="radio" value="chapter" /><label class="radio" for="type3">Capitolo</label>
					<input id="type4" name="type" type="radio" value="book" /><label class="radio" for="type4">Libro</label>
				</div>
				
				<div class="article documentInfo control-group visible"><label for="article_name">Titolo rivista</label>
				<input id="article_name" name="article_name" type="text" size="30" maxlength="255" /></div>
				
				
				<div class="article documentInfo control-group visible"><label for="article_volume">Volume</label>
				<input id="article_volume" name="article_volume" type="text" size="3" maxlength="3" /></div>
				
				
				<div class="article documentInfo control-group visible"><label for="article_issue">Edizione</label>
				<input id="article_issue" name="article_issue" type="text" size="3" maxlength="3" /></div>
				
				
				<div class="article documentInfo control-group visible"><label for="article_start_page">Pagina iniziale</label>
				<input id="article_start_page" name="article_start_page" type="text" size="4" maxlength="4" /></div>
				
				
				<div class="article documentInfo control-group visible"><label for="article_end_page">Pagina finale</label>
				<input id="article_end_page" name="article_end_page" type="text" size="4" maxlength="4" /></div>



				<div class="act documentInfo control-group hidden"><label for="act_name">Nome Conferenza</label>
				<input id="act_name" name="act_name" type="text" size="30" maxlength="255" /></div>
				

				<div class="act documentInfo control-group hidden"><label for="act_location">Location</label>
				<input id="act_location" name="act_location" type="text" size="30" maxlength="255" /></div>
				

				<div class="act documentInfo control-group hidden"><label for="act_date">Data</label>
				<input id="act_date" name="act_date" type="text" size="10" maxlength="10" /></div>
				
				
				<div class="act documentInfo control-group hidden"><label for="act_start_page">Pagina iniziale</label>
				<input id="act_start_page" name="act_start_page" type="text" size="4" maxlength="4" /></div>
				

				<div class="act documentInfo control-group hidden"><label for="act_end_page">Pagina finale</label>
				<input id="act_end_page" name="act_end_page" type="text" size="4" maxlength="4" /></div>
				

			
				<div class="book documentInfo control-group hidden"><label for="book_editor">Casa editrice</label>
				<input id="book_editor" name="book_editor" type="text" size="30" maxlength="255" /></div>
				
				<div class="book documentInfo control-group hidden"><label for="book_edition">Edizione</label>
				<input id="book_edition" name="book_edition" type="text" size="2" maxlength="2" /></div>
				

			
				<div class="chapter documentInfo control-group hidden"><label for="chapter_name">Titolo libro</label>
				<input id="chapter_name" name="chapter_name" type="text" size="30" maxlength="255" /></div>
				

				<div class="chapter documentInfo control-group hidden"><label for="chapter_editor">Casa editrice</label>
				<input id="chapter_editor" name="chapter_editor" type="text" size="30" maxlength="255" /></div>
				

				<div class="chapter documentInfo control-group hidden"><label for="chapter_curators">Curatori del libro</label>
				<input id="chapter_curators" name="chapter_curators" type="text" size="30" maxlength="255" /></div>
				

				<div class="chapter documentInfo control-group hidden"><label for="chapter_start_page">Pagina iniziale</label>
				<input id="chapter_start_page" name="chapter_start_page" type="text" size="4" maxlength="4" /></div>
				

				<div class="chapter documentInfo control-group hidden"><label for="chapter_end_page">Pagina finale</label>
				<input id="chapter_end_page" name="chapter_end_page" type="text" size="4" maxlength="4" /></div>
				
			</div>
			<div class="controls" style="clear:both; width:100%;">
				<input class="button button-primary" type = "submit" value ="Invia dati" />
				<input class="button" type = "reset" value = "Reset" />
			</div>
		</fieldset>
	</form>
</div>
<!-- END CONTENTS -->
<script type="text/javascript">
//<![CDATA[
$( document ).ready(function() {

	$('#createDocument').bind('keypress keydown keyup', function(e){
	   if(e.keyCode == 13) { e.preventDefault(); }
	});

	$('input[name="type"]').on('change', function() {
		switch($( "input[name='type']:checked" ).val())
		{
			case 'act':
				$('.documentInfo.visible').addClass('hidden').removeClass('visible');
				$('.act').removeClass('hidden').addClass('visible').css('opacity', '0').animate({opacity: '1',height: 'auto'}, 1000);
			break;

			case 'article':
				$('.documentInfo.visible').addClass('hidden').removeClass('visible');
				$('.article').removeClass('hidden').addClass('visible').css('opacity', '0').animate({opacity: '1',height: 'auto'}, 1000);
			break;

			case 'book':
				$('.documentInfo.visible').addClass('hidden').removeClass('visible');
				$('.book').removeClass('hidden').addClass('visible').css('opacity', '0').animate({opacity: '1',height: 'auto'}, 1000);
			break;

			case 'chapter':
				$('.documentInfo.visible').addClass('hidden').removeClass('visible');
				$('.chapter').removeClass('hidden').addClass('visible').css('opacity', '0').animate({opacity: '1',height: 'auto'}, 1000);
			break;
		}
	});

	$('#tags').tokenize({
	    datas: "<?php echo $this->createLink('tag', 'ajax', 'search'); ?>",
	    searchParam: 'q',
	    displayDropdownOnFocus: true,
	    beforeAddNewToken:	function(value, text){
	    	var newId = null;
	    	if(value == text) {
	    		//add tag to db
	    		$.ajax({
	    			url: "<?php echo $this->createLink('tag', 'ajax', 'create'); ?>/" + value,
	    			async: false,
	    			success: function(result){
	    				newId = JSON.parse(result);
	    			}
	    		});
	    	}
	    	return newId;
	    }
	});

	$("#createDocument").validate(  
	{  
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		},
		highlight: function(element, errorClass) {
			$(element).addClass('error');
			$(element).next().removeClass("checked");
		},
	    rules:
	    {
    	'title':{
    		required: true,
    		maxlength: 100
			},
		'author':{
			required: true,
    		minlength: 2,
    		maxlength: 100
			},
		'year':{
			required: true,
			},
		'url':{
			required: true,
			url: true,
			maxlength: 255
			},
		'comment':{
			minlength: 3,
    		maxlength: 255
			},
		'act_name':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'act_location':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'act_date':{
			required: true
			},
		'act_start_page':{
			required: true,
			digits: true
			},
		'act_end_page':{
			required: true,
			digits: true,
			lessThanEqual: 'act_start_page'
			},
		'article_name':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'article_volume':{
			digits: true,
			maxlength: 3
			},
		'article_issue':{
			digits: true,
			maxlength: 3
			},
		'article_start_page':{
			required: true,
			digits: true
			},
		'article_end_page':{
			required: true,
			digits: true,
			lessThanEqual: 'article_start_page'
			},
		'book_editor':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'book_edition':{
			required: true,
			digits: true,
    		maxlength: 2
			},
		'chapter_name':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'chapter_editor':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'chapter_curators':{
			required: true,
			minlength: 3,
    		maxlength: 100
			},
		'chapter_start_page':{
			required: true,
			digits: true
			},
		'chapter_end_page':{
			required: true,
			digits: true,
			lessThanEqual: 'chapter_start_page'
			}
	    }, 
	    messages:  
	    {  
	    'title':{
	    	required: 'Il campo titolo è obbligatorio.',
	    	maxlength: 'Inserisci un titolo di massimo 3 caratteri.'
			},
		'author':{
			required: 'Il campo autore è obbligatorio.',
	    	minlength: 'Inserisci un autore di almeno 2 caratteri.'
			},
		'year':{
			required: 'Il campo anno è obbligatorio.',
	    	},
		'url':{
			required: 'Il campo url è obbligatorio.',
			url: 'Inserisci un url valido.',
			maxlength: 'Inserisci un url di massimo 255 caratteri.'
			},
		'comment':{
			minlength: 'Inserisci un commento di almeno 3 caratteri.',
    		maxlength: 'Inserisci un commento di massimo 255 caratteri.'
			},
		'act_name':{
			required: 'Il campo nome atto è obbligatorio.',
			minlength: 'Inserisci un nome atto di almeno 3 caratteri.',
			maxlength: 'Inserisci un nome atto di massimo 100 caratteri.'
			},
		'act_location':{
			required: 'Il campo location è obbligatorio.',
			minlength: 'Inserisci una location di almeno 3 caratteri.',
    		maxlength: 'Inserisci una location di massimo 100 caratteri.'
			},
		'act_date':{
			required: 'Il campo data è obbligatorio.'
			},
		'act_start_page':{
			required: 'Il campo pagina iniziale è obbligatorio.',
			digits: 'Il campo pagina iniziale deve essere composta da sole cifre.'
			},
		'act_end_page':{
			required: 'Il campo pagina finale è obbligatorio.',
			digits: 'Il campo pagina finale deve essere composta da sole cifre.',
			lessThanEqual: 'Il campo pagina finale deve essere maggiore o uguale a quello di pagina iniziale.'
			},
		'article_name':{
			required: 'Il campo titolo rivista è obbligatorio',
			minlength: 'Inserisci un titolo rivista di almeno 3 caratteri.',
    		maxlength: 'Inserisci un titolo rivista di massimo 3 caratteri.'
			},
		'article_volume':{
			digits: 'Il campo volume deve essere composto da sole cifre.',
			maxlength: 'Inserisci un volume di massimo 3 caratteri.'
			},
		'article_issue':{
			digits: 'Il campo edizione deve essere composto da sole cifre.',
			maxlength: 'Inserisci un\'edizione di massimo 3 caratteri.'
			},
		'article_start_page':{
			required: 'Il campo pagina iniziale è obbligatorio.',
			digits: 'Il campo pagina iniziale deve essere composta da sole cifre.'
			},
		'article_end_page':{
			required: 'Il campo pagina finale è obbligatorio.',
			digits: 'Il campo pagina finale deve essere composta da sole cifre.',
			lessThanEqual: 'Il campo pagina finale deve essere maggiore o uguale a quello di pagina iniziale.'
			},
		'book_editor':{
			required: 'Il campo casa editrice è obbligatorio.',
			minlength: 'Inserisci una casa editrice di almeno 3 caratteri.',
    		maxlength: 'Inserisci una casa editrice di massimo 100 caratteri.'
			},
		'book_edition':{
			required: 'Il campo edizione è obbligatorio.',
			digits: 'Il campo edizione deve essere composta da sole cifre.',
    		maxlength: 'Inserisci un\'edizione di massimo 2 caratteri.'
			},
		'chapter_name':{
			required: 'Il campo titolo libro è obbligatorio.',
			minlength: 'Inserisci un titolo libro di almeno 3 caratteri.',
    		maxlength: 'Inserisci un titolo libro di massimo 100 caratteri.'
			},
		'chapter_editor':{
			required: 'Il campo casa editrice libro è obbligatorio.',
			minlength: 'Inserisci una casa editrice di almeno 3 caratteri.',
    		maxlength: 'Inserisci una casa editrice di massimo 100 caratteri.'
			},
		'chapter_curators':{
			required: 'Il campo curatori del libro libro è obbligatorio.',
			minlength: 'Inserisci curatori del libro di almeno 3 caratteri.',
    		maxlength: 'Inserisci curatori del libro di massimo 100 caratteri.'
			},
		'chapter_start_page':{
			required: 'Il campo pagina iniziale è obbligatorio.',
			digits: 'Il campo pagina iniziale deve essere composta da sole cifre'
			},
		'chapter_end_page':{
			required: 'Il campo pagina finale è obbligatorio.',
			digits: 'Il campo pagina finale deve essere composta da sole cifre.',
			lessThanEqual: 'Il campo pagina finale deve essere maggiore o uguale a quello di pagina iniziale.'
			}
	    }
	});
	$("#act_date").datepicker($.datepicker.regional[ "it" ] );
	yearSelect();
});

//]]>
</script>
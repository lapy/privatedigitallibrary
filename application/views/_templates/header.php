<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
    <!-- head section  -->
    <link rel="stylesheet" type ="text/css" href="<?php echo URL; ?>/public/css/jquery-ui.min.css"/>
    <link rel="stylesheet" type ="text/css" href="<?php echo URL; ?>/public/css/jquery-ui.structure.min.css"/>
    <link rel="stylesheet" type ="text/css" href="<?php echo URL; ?>/public/css/jquery-ui.theme.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>/public/css/jquery.tokenize.css" />
    <link rel="stylesheet" type ="text/css" href="<?php echo URL; ?>/public/css/style.css"/>
    <script type="text/javascript" src="<?php echo URL; ?>/public/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/js/jquery.jqpagination.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/js/jquery.tokenize.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/js/application.js"></script>
    <script type="text/javascript">var URL = '<?php echo URL; ?>';</script>
    <title>Private Digital Library</title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
<!-- body section -->
    <div id="viewport">
    <div id="header">
        <div id="logo"><a href="<?php echo URL; ?>"><img src="<?php echo URL; ?>/public/css/images/logo.png" alt="logo sito"/></a></div>
        <div id="mainmenu">
            <ul id="openmenu">
                <li>
                    <a href="#"><img src="<?php echo URL; ?>/public/css/images/arrow.png" alt="icona menu piccolo" /></a>
                    <ul id="documentsmenuvertical">
                        <li><a href="<?php echo $this->createLink('document'); ?>">Documenti</a></li>
                        <li><a href="<?php echo $this->createLink('article'); ?>">Articoli</a></li>
                        <li><a href="<?php echo $this->createLink('act'); ?>">Atti</a></li>
                        <li><a href="<?php echo $this->createLink('chapter'); ?>">Capitoli</a></li>
                        <li><a href="<?php echo $this->createLink('book'); ?>">Libri</a></li>
                        <?php if ($this->isUserLogged()) {?>
                        <li class="newdocumentli"><a class="newdocument" href="<?php echo $this->createLink('document', 'create'); ?>">+ Nuovo</a></li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
            <ul id="documentsmenu">
                <li><a href="<?php echo $this->createLink('document'); ?>">Documenti</a></li>
                <li><a href="<?php echo $this->createLink('article'); ?>">Articoli</a></li>
                <li><a href="<?php echo $this->createLink('act'); ?>">Atti</a></li>
                <li><a href="<?php echo $this->createLink('chapter'); ?>">Capitoli</a></li>
                <li><a href="<?php echo $this->createLink('book'); ?>">Libri</a></li>
                <?php if ($this->isUserLogged()) {?>
                <li class="newdocumentli"><a class="newdocument" href="<?php echo $this->createLink('document', 'create'); ?>">+ Nuovo</a></li>
                <?php } ?>
            </ul>
            <?php if (!$this->isUserLogged()) {?>
            <ul id="loginsmall">
                <li>
                    <a class="loginlink" href="#">Login</a>
                </li>
            </ul>
            <?php } ?>
            <?php if ($this->isUserLogged()) {?>
            <ul id="controlpanelmenu">
                <li class="top">
                    <a href="#">
                        <img src="<?php echo URL; ?>/public/css/images/menu_icon.png" alt="icona menu" height="36" width="36" />
                        <?php if ($notificationnumber = count($this->pendingEdits)) {?>
                            <span class="notificationnumber" style="position: absolute; top: 3.45px; right: 10px;"><?php echo $notificationnumber; ?></span>
                        <?php } ?>
                    </a>
                    <ul id="controlpanelmenuinner">
                        <li id="loginsmall"><span class="welcome">Benvenuto, <?php echo $_SESSION['user']->username; ?></span></li>
                        <li><a href="<?php echo $this->createLink('user', 'profile'); ?>"><span class="ui-icon ui-icon-person"></span>Il mio account</a></li>
                        <li><a href="<?php echo $this->createLink('user', 'edit'); ?>"><span class="ui-icon ui-icon-wrench"></span>Modifica Credenziali</a></li>
                        <?php if ($this->isUserManager()) {?>
                        <li><a href="<?php echo $this->createLink('user'); ?>"><span class="ui-icon ui-icon-contact"></span>Lista Utenti</a></li>
                        <li><a href="<?php echo $this->createLink('user', 'insert'); ?>"><span class="ui-icon ui-icon-plus"></span>Invita Utente</a></li>
                        <?php } ?>
                        <?php if ($this->pendingEdits) {?>
                        <li><a href="<?php echo $this->createLink('user', 'pendingEdits'); ?>"><span class="ui-icon ui-icon-clock"></span>Modifiche in attesa&nbsp;<span class="notificationnumber"><?php echo count($this->pendingEdits); ?></span></a></li>
                        <?php } ?>
                        <li><a href="<?php echo $this->createLink('auth','logout'); ?>"><span class="ui-icon ui-icon-arrowthickstop-1-e"></span>Logout</a></li>
                    </ul>
                </li>
            </ul>
            <?php } ?>
            <ul id="loginmenu">
                <?php if (!$this->isUserLogged()) {?>
                <li><a class="loginlink" href="#">Login</a></li>
                <?php } else { ?>
                <li id="loginmenuli"><span class="welcome">Benvenuto, <?php echo $_SESSION['user']->username; ?></span></li>
                <?php } ?>
            </ul>
        </div>
        <div id="login">
            <?php if (!$this->isUserLogged()) {?>
            <form class="stacked" action="<?php echo $this->createLink('auth','login'); ?>" method="post">
                <div class="control-group"><p style="padding:2px 0;"><label for="username">Username</label>
                    <input id="username" name="username" type="text" style="width:187px;" /></p></div>

                <div class="control-group"><p style="padding:2px 0;"><label for="password">Password</label>
                    <input id="password" name="password" type="password" style="width:187px;" /></p></div>

                    <p style="padding:0;"><input name="redirect" type="hidden" value="<?php echo "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" /></p>

                <div class="controls">
                    <p style="padding:7px 0 0 0; font-size:13px;">
                        <input class="button button-primary" type="submit" value="Login" /> &nbsp;
                        <span><a href="<?php echo $this->createLink('auth', 'lostPassword'); ?>">Password smarrita?</a></span>
                    </p>
                </div>
            </form>
            
            <?php } ?>
        </div>
    </div>
    <!-- BEGIN MAIN SECTION -->
    <div id="main">
<div class="editControls">

<?php if($document->edit_of) { $originalDoc = Document::find($document->edit_of);?>

	<p class="modified_by">Questa è una modifica proposta da <a href="<?php echo $this->createLink('user','profile',Document::find($document->edit_of)->edit_user->username); ?>"><?php echo end($originalDoc->modification_tokens)->edit_user->username; ?></a></p>

<?php 	/* se il visistatore è l'utente che ha inserito il documento originale */ 
		if(!$this->isUserManager() && $_SESSION['user']->id == $originalDoc->user_id) {
			if($token = end($originalDoc->modification_tokens)->user_token) { ?>
	
	<a class="button button-primary acceptedit" href="<?php echo $this->createLink('document', 'editAccept', 'user/'.$token ); ?>">Accetta le modifiche</a>
	<a class="button refuseedit" href="<?php echo $this->createLink('document', 'editRefuse', 'user/'.$token ); ?>">Rifiuta le modifiche</a>

<?php }/* se il visistatore è l'admin */ 
		} else if($this->isUserManager()) {
			if($token = end($originalDoc->modification_tokens)->admin_token) { ?>

	<a class="button button-primary acceptedit" href="<?php echo $this->createLink('document', 'editAccept', 'manager/'.$token ); ?>">Accetta le modifiche</a>
	<a class="button refuseedit" href="<?php echo $this->createLink('document', 'editRefuse', 'manager/'.$token ); ?>">Rifiuta le modifiche</a>

<?php } } } ?>

</div>
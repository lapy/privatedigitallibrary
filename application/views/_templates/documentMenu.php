<ul id="documentmenu" class="button button-primary">
	<li>
		<a href="#">Opzioni</a>
		<ul>
			<?php if(empty(end($document->modification_tokens)->admin_token) && empty(end($document->modification_tokens)->admin_token)) { ?>
			<li class="button edit">
				<a class="edit" href="<?php echo $this->createLink($document->type, 'edit', $document->id ); ?>">
					<span class="ui-icon ui-icon-pencil"></span>
					<span class="text">Modifica</span>
				</a>
			</li>
			<?php } else { ?>
			<li class="button button-disabled" style="opacity: 0.9;">
					<span class="ui-icon ui-icon-clock"></span>
					<span class="text" style="color: gray;">Modifica</span>
			</li>
			<?php } ?>
			<?php if($this->isUserManager()) { ?>
			<li class="button delete">
				<a class="delete" href="<?php echo $this->createLink('document', 'delete', $document->id ); ?>">
					<span class="ui-icon ui-icon-trash"></span>
					<span class="text">Elimina</span>
				</a>
			</li>
			<?php } ?>
			<li class="button mail">
				<a href="mailto:?subject=Documento%20su%20PrivateDigitalLibrary&amp;body=Questo%20documento%20potrebbe%20interessarti%3A%0A<?php echo $this->createLink($document->type, 'view', $document->id ); ?>">
					<span class="ui-icon ui-icon-mail-closed"></span>
					<span class="text">Condividi</span>
				</a>
			</li>
			<li class="button xml">
				<a href="<?php echo $this->createLink('document', 'getXml', $document->id ); ?>">
					<span class="ui-icon ui-icon-script"></span>
					<span class="text">XML</span>
				</a>
			</li>
		</ul>
	</li>	
</ul>
<div id="dialog"></div>
<div id="dialog2"></div>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {

	$("#dialog").dialog({
		modal: true,
		bgiframe: true,
		width: 310,
		height: 250,
		autoOpen: false,
		title: 'Eliminazione Documento'
	});
	$("#dialog2").dialog({
		modal: true,
		bgiframe: true,
		width: 300,
		height: 270,
		autoOpen: false,
		title: 'Modifica Documento'
	});

	// LINK
	$('a.delete').click(function(e) {
		e.preventDefault();
        var theHREF = $(this).attr("href");
		var theREL = $(this).attr("rel");
		var theMESSAGE = 'Sei sicuro di voler eliminare questo documento?';
		var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
		
		// set windows content
		$('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');
		
		$("#dialog").dialog('option', 'buttons', {
                "Conferma" : function() {
                    window.location.href = theHREF;
                    },
                "Annulla" : function() {
                    $(this).dialog("close");
                    }
                });
		$("#dialog").dialog("open");
	});

<?php if($_SESSION['user']->id != $document->user_id) { ?>
	// LINK
	$('a.edit').click(function(e) {
		e.preventDefault();
        var theHREF = $(this).attr("href");
		var theREL = $(this).attr("rel");
		var theMESSAGE = "Le tue modifiche saranno proposte all'utente che ha inserito questo documento e al responsabile. Solo dopo che entrambi avranno accettato le tue modifiche, esse saranno applicate. Vuoi continuare?";
		var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
		
		// set windows content
		$('#dialog2').html('<P>' + theICON + theMESSAGE + '</P>');
		
		$("#dialog2").dialog('option', 'buttons', {
                "Si" : function() {
                    window.location.href = theHREF;
                    },
                "Annulla" : function() {
                    $(this).dialog("close");
                    }
                });
		$("#dialog2").dialog("open");
	});
<?php } ?>
});
//]]>
</script>
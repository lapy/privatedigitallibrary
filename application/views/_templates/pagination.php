<div class="paginationwrapper" >
	<div class="pagination" >
	    <a href="#" class="first" rel="first">&laquo;</a>
	    <a href="#" class="previous" rel="previous">&lsaquo;</a>
	    <input title="pagenumber" type="text" readonly="readonly"/>
	    <a href="#" class="next" rel="next">&rsaquo;</a>
	    <a href="#" class="last" rel="last">&raquo;</a>
	</div>
</div>
<script type="text/javascript">
//<![CDATA[
pagination();
//]]>
</script>
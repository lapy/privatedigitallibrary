<div class="comparison">
	<h1 class="title">Modificato</h1>
	<div class="document">
		<div class="field title"><div class="label">Titolo: </div><?php echo $document->title; ?></div>
		<div class="field author"><div class="label">Autore: </div><?php echo $document->author; ?></div>
		<div class="field year"><div class="label">Anno: </div><?php echo $document->year; ?></div>
		<div class="field url"><div class="label">Url: </div><?php echo '<a href="'.$document->url.'">'.$document->url.'</a>'; ?></div>
		<div class="field status"><div class="label">Visibilità: </div>
			<?php 
				if($document->status==0)
					echo 'Privata';
				else
					echo 'Pubblica';
			?>
		</div>
		<div class="field taglist"><div class="label">Tag: </div><?php foreach ($document->tags as $tag) {
			echo '<span class="tag"><a href="' . $this->createLink('document', 'tag', $tag->name) . '">' . $tag->name . '</a></span>';
		} ?></div>
		<div class="field comment"><div class="label">Commenti: </div><?php echo $document->comment; ?></div>

		<div class="field name"><div class="label">Nome Conferenza: </div><?php echo $document->info->name; ?></div>
		<div class="field location"><div class="label">Location: </div><?php echo $document->info->location; ?></div>
		<div class="field date"><div class="label">Data: </div><?php if(isset($document->info->date) && is_a($document->info->date, 'ActiveRecord\DateTime')) echo $document->info->date->format('d/m/Y'); ?></div>
		<div class="field pages"><div class="label">Dimensione: </div>
		<?php if ($document->info->end_page == $document->info->start_page) { ?> 1 pagina, pag. <?php echo $document->info->start_page; }
		else { echo ($document->info->end_page - $document->info->start_page + 1); ?> pagine, da pag. <?php echo $document->info->start_page; ?> a pag. <?php echo $document->info->end_page; } ?></div>
		<!--
		<div class="field act_start_page"><div class="label">Pagina Iniziale: </div><?php echo $document->info->start_page; ?></div>
		<div class="field act_end_page"><div class="label">Pagina Finale: </div><?php echo $document->info->end_page; ?></div>
		-->
		<div class="field createdBy"><div class="label">Inserito da: </div><a href="<?php echo $this->createLink('user','profile',$document->user->username); ?>"><?php echo $document->user->username; ?></a></div>
	</div>
</div>
</div>
<!-- END CONTENTS -->
<?php

/**
 * Configurazione
 *
 */

/**
 * Configuration for: Error reporting
 * Useful to show every little problem during development, but only show hard errors in production
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);

/**
 * Configuration for: URL
 * Here we auto-detect your applications URL and the potential sub-folder. Works perfectly on most servers and in local
 * development environments (like WAMP, MAMP, etc.). Don't touch this unless you know what you do.
 *
 * URL_PUBLIC_FOLDER:
 * The folder that is visible to public, users will only have access to that folder so nobody can have a look into
 * "/application" or other folder inside your application or call any other .php file than index.php inside "/public".
 *
 * URL_PROTOCOL:
 * The protocol. Don't change unless you know exactly what you do.
 *
 * URL_DOMAIN:
 * The domain. Don't change unless you know exactly what you do.
 *
 * URL_SUB_FOLDER:
 * The sub-folder. Leave it like it is, even if you don't use a sub-folder (then this will be just "/").
 *
 * URL:
 * The final, auto-detected URL (build via the segments above). If you don't want to use auto-detection,
 * then replace this line with full URL (and sub-folder) and a trailing slash.
 */

define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', '//');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);

/**
 * Configuration for: Database
 *
 */
define('DB_TYPE', 'mysql');
define('DB_HOST', 'techrelated.it');
define('DB_NAME', 'pdldb');
define('DB_USER', 'pdldb');
define('DB_PASS', 'root');

/**
 * Configuration for: ActiveRecord
 * 
 */

ActiveRecord\Config::initialize(function($cfg)
{
   $cfg->set_model_directory(APP . 'models');
   $cfg->set_connections(array('development' =>
     'mysql://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME.'?charset=utf8'));
});

/**
 * Configuration for: ZebraSession
 * 
 */
 
// try to connect to the MySQL server
$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Could not connect to database!');
$session = new Zebra_Session($db, 'tr1togue$$th1ss@alt', 3600);

/**
 * Configuration for: PHPMailer
 * 
 */

define('SMTP_HOST', 'techrelated.it');
define('SMTP_AUTH', true);
define('SMTP_USERNAME', 'privatedigitallibrary@techrelated.it');
define('SMTP_PASSWORD', 'daganivi');
define('SMTP_SECURE', '');
define('SMTP_PORT', 25);
define('SMTP_FROM', 'privatedigitallibrary@techrelated.it');
define('SMTP_FROMNAME', 'Private Digital Library');

<?php

/**
 * Class actController
 *
 */
class actController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/act/index (which is the default page btw)
     */
    public function index()
    {
        require APP . 'views/_templates/header.php';
        require APP . 'views/act/index.php';
        require APP . 'views/_templates/pagination.php';
        require APP . 'views/_templates/footer.php';
    }

    public function view($id)
    {
        try
        {
            $document = Document::find($id);
            $document->info;
            
            // se l'utente è loggato o il doc è pubblico
            if($this->isUserLogged() || $document->status == 1)
            {
                require APP . 'views/_templates/header.php';
                if($document->edit_of)
                {
                    $originalDocument = Document::find($document->edit_of);
                    require APP . 'views/act/originalView.php';
                    require APP . 'views/act/editView.php';
                }
                else
                    require APP . 'views/act/view.php';
                if($this->isUserLogged()) {
                    if(!$document->edit_of)
                        require APP . 'views/_templates/documentMenu.php';
                    require APP . 'views/_templates/editControls.php';
                }
                require APP . 'views/_templates/footer.php';
            }
            else
            {
                // accesso non consentito
                require APP . 'controllers/errorController.php';
                $page = new errorController();
                $page->restrictedAccess();
            }
        } 
        catch (Exception $e)
        {
            // not found: show the error page
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->index();
            
        }
    }

    public function edit($id)
    {
        if($this->isUserLogged())
        {
            try
            {
                $document = Document::find($id);

                if(!empty($_POST))
                {
                    if($document->user_id != $_SESSION['user']->id)
                    {
                        $newDocument = new Document();
                        $newDocument->user_id = $document->user_id;
                        $newDocument->comment = $_POST['comment'];
                        $newDocument->status = $_POST['status'];
                        $newDocument->author = $_POST['author'];
                        $newDocument->title = $_POST['title'];
                        $newDocument->year = $_POST['year'];
                        $newDocument->url = $_POST['url'];
                        $newDocument->type = $document->type;
                        $newDocument->edit_of = $id;

                        try {
                            $newDocument->save();
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);  
                        }
                        
                        if(isset($_POST['tags']))
                        foreach ($_POST['tags'] as $key => $value) {
                            Has_tag::create( array(
                                'document_id' => $newDocument->id,
                                'tag_id' => $value
                            ));
                        }

                        $newInfo = new Act();
                        $newInfo->document_id = $newDocument->id;
                        $newInfo->name = $_POST['act_name'];
                        $newInfo->location = $_POST['act_location'];
                        $newInfo->date = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['act_date'])));
                        $newInfo->start_page = $_POST['act_start_page'];
                        $newInfo->end_page = $_POST['act_end_page'];
                        
                        try {
                            $newInfo->save();
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);
                        }
                        

                        $modificationToken = new Document_modification_token();
                        $modificationToken->document_id = $id;
                        $modificationToken->user_id = $document->user_id;
                        $modificationToken->edit_user_id = $_SESSION['user']->id;
                        $modificationToken->admin_token = $this->generateRandomString();
                        $modificationToken->user_token = $this->generateRandomString();
                        
                        try {
                            $modificationToken->save();
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);
                        }
                        

                        $this->mail->addAddress($document->user->email, $document->user->name . ' ' . $document->user->surname);
                        if($document->user->role != 0)
                        {
                            $manager = User::find_by_role(0);
                            $this->mail->addAddress($manager->email, $manager->name . ' ' . $manager->surname);
                        }
                        $this->mail->isHTML(true);
                        $this->mail->Subject = 'Proposta modifiche di un documento su PrivateDigitalLibrary';
                        $this->mail->Body = '<p>Ciao, <i>' . $_SESSION['user']->username . '</i> ha proposto delle modifiche su un tuo documento.<br />';
                        $this->mail->Body .= 'Per visionare le modifiche clicca sul link seguente:<br /><br /></p>';
                        $this->mail->Body .= '<a href="' . $this->createLink($newDocument->type, 'view', $newDocument->id) . '">';
                        $this->mail->Body .= $this->createLink($newDocument->type, 'view', $newDocument->id) . '</a>';

                        if(!$this->mail->send())
                        {
                            $mailMessage = 'Impossibile inviare l\'email.';
                            $mailMessage .= " Errore: " . $mail->ErrorInfo;
                            // load views.
                            require APP . 'views/_templates/header.php';
                            echo '<div class="contents"><p class="warning">'.$mailMessage.'</p></div>';
                            require APP . 'views/_templates/footer.php';
                        }
                        else{
                            // load views.
                            require APP . 'views/_templates/header.php';
                            echo '<div class="contents"><p class="confirmation">Le tue modifiche sono state proposte.</p></div>';
                            require APP . 'views/_templates/footer.php';
                        }
                    }
                    else
                    {
                        $document->comment = $_POST['comment'];
                        $document->status = $_POST['status'];
                        $document->author = $_POST['author'];
                        $document->title = $_POST['title'];
                        $document->year = $_POST['year'];
                        $document->url = $_POST['url'];
                        
                        try {
                            $document->save();
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);
                            
                        }
                        

                        Document_modification_token::table()->delete(array('document_id' => $id));

                        Has_tag::table()->delete(array('document_id' => $id));
                        if(isset($_POST['tags']))
                        foreach ($_POST['tags'] as $key => $value) {
                            Has_tag::create( array(
                                
                                'document_id' => $document->id,
                                'tag_id' => $value
                            ));
                        }

                        try {
                            $info = Act::find($id);
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);
                            
                        }
                        
                        $info->name = $_POST['act_name'];
                        $info->location = $_POST['act_location'];
                        $info->date = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['act_date'])));
                        $info->start_page = $_POST['act_start_page'];
                        $info->end_page = $_POST['act_end_page'];
                        
                        try {
                            $info->save();
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);
                            
                        }
                        

                        // load views.
                        require APP . 'views/_templates/header.php';
                        echo '<div class="contents"><p class="confirmation">Le tue modifiche sono state applicate. Sarai redirezionato ai dettagli fra 5 secondi.</p></div>';
                        echo "<script type=\"text/javascript\">setTimeout(function () {window.location.href='".$this->createLink($document->type, 'view', $document->id)."';},5000);</script>";
                        require APP . 'views/_templates/footer.php';
                    }     
                }
                else
                {
                    require APP . 'views/_templates/header.php';
                    require APP . 'views/act/edit.php';
                    require APP . 'views/_templates/footer.php';
                } 
            }
            catch (Exception $e)
            {
                // not found: show the error page
                require APP . 'controllers/errorController.php';
                $page = new errorController();
                $page->index();
            }
        }
        else
        {
            // accesso non consentito
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->restrictedAccess();
        }
    }

    public function ajax($pageNumber = 1, $elementsPerPage = 2, $searchString = null)
    {
        $json = null;
        $offset = ($pageNumber-1) * $elementsPerPage;
        $getmaxelements = isset($_GET['maxelements']) ? true : false;
        
        //Se non è una ricerca
        if($searchString == null)
        {
            if(!$this->isUserLogged()) {
                $numElements = Document::find_all_by_type_and_status_and_edit_of('act',1,null, array(
                    'select' => 'count(*) AS num_rows'
                ))[0]->num_rows;
                if($getmaxelements) { echo json_encode($numElements); die(); }
                $documents = Document::find_all_by_type_and_status_and_edit_of('act',1,null, array(
                    'limit' => $elementsPerPage,
                    'offset' => $offset
                ));
            }
            else 
            {
                $numElements = Document::find_all_by_type_and_edit_of('act',null, array(
                    'select' => 'count(*) AS num_rows'
                ))[0]->num_rows;
                if($getmaxelements) { echo json_encode($numElements); die(); }
                $documents = Document::find_all_by_type_and_edit_of('act',null, array(
                    'limit' => $elementsPerPage,
                    'offset' => $offset
                ));
            }
        }
        //se è una ricerca
        else
        {
            $searchString = strtolower($searchString);
            $searchString = explode('-',$searchString);
            $query = '';
            foreach ($searchString as $key => $value) {
                $query .= '(';
                foreach(Document::connection()->columns('documents') as $attribute)
                {
                    if($attribute->name != 'status')
                        $query .= '`' . $attribute->name . '`' . " LIKE '%".$value."%' OR ";

                }
                foreach(Act::connection()->columns('acts') as $attribute)
                {
                   $query .= '`' . $attribute->name . '`' . " LIKE '%".$value."%' OR ";
                }
                //elimino l'OR in eccesso
                $query = substr($query, 0, -3);
                $query .= ') AND ';
            }
            
            if(!$this->isUserLogged())
                //solo documenti pubblici
                $query .= '`status` = 1';
            else
                //elimino l'AND in eccesso
                $query = substr($query, 0, -4);
            $join = 'INNER JOIN  acts ON (documents.id = acts.document_id)';
            $numElements = Document::all('joins', array(
                'joins' => $join,
                'select' => 'count(*) AS num_rows',
                'conditions' => array($query . ' AND edit_of IS ?', null)
            ))[0]->num_rows;
            if($getmaxelements) { echo json_encode($numElements); die(); }
            $documents = Document::all('joins', array(
                'joins' => $join,
                'limit' => $elementsPerPage,
                'offset' => $offset,
                'conditions' => array($query . ' AND edit_of IS ?', null),
            ));
            //echo Document::connection()->last_query;
        }
        //riempio il json
        $json['numElements'] = $numElements;
        $json['max_page'] = ceil($numElements/$elementsPerPage);
        foreach ( $documents as $document )
        {
            $result = null;
            $result[]='<a href="'.$this->createLink('act','view',$document->id).'">'.$document->title.'</a>';
            $result[]=$document->info->name;
            $result[]=$document->author;
            $result[]=$document->year;
            $result[]=$document->info->location;
            if (isset($document->info->date) && is_a($document->info->date, 'ActiveRecord\DateTime')) 
            {
                $result[]=$document->info->date->format('d/m/Y');
            }
            if(isset($document->created_at) && is_a($document->created_at, 'ActiveRecord\DateTime'))
            {
                $result[]=$document->created_at->format('d/m/Y');
            } 
            else 
            {
                $result[]='-';
            }
            if($document->status == 0)
                $result[]='Privata';
            else
                $result[]='Pubblica';
            $json['results'][] = $result;
        }
        header('ContentType: application/json');
        echo json_encode($json);
    }
}
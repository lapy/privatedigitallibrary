<?php

/**
 * Class tagController
 *
 */
class tagController extends Controller
{
    public function ajax($operation = null, $value = null)
    {
        if($operation == 'search' && isset($_GET['q']))
        {
            $tags = Tag::find('all', array('conditions' => array('name LIKE ?', '%'.$_GET['q'].'%')));
            $tagsArray = array();
            foreach ($tags as $tag) 
                $tagsArray[] = array('value' => $tag->id, 'text' => $tag->name);
            $tagsArray = json_encode($tagsArray);
            echo $tagsArray;
        }
        else if($operation == 'create' && isset($value))
        {
            $tag = new Tag;
            $tag->name = $value;
            $tag->save();
            header('ContentType: application/json');
            echo json_encode($tag->id);
        }
        else
            echo 'error';
    }
}

<?php

/**
 * Class homeController
 *
 */
class homeController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {
        // load views
        require APP . 'views/_templates/header.php';
        require APP . 'views/home/index.php';
        require APP . 'views/_templates/footer.php';
    }

    public function ajax($type = 'documentsTable')
    {
        switch($type)
        {
            case 'documentsTable':
                $json = null;
                if(!$this->isUserLogged()) {
                    $documents = Document::find('all', array(
                        'limit' => 5,
                        'offset' => 0,
                        'conditions' => array('status = ? AND edit_of IS ?', 1, null),
                        'order' => 'created_at desc'
                    ));
                }
                else {
                    $documents = Document::find('all', array(
                        'limit' => 5,
                        'offset' => 0,
                        'conditions' => array('edit_of IS ?', null),
                        'order' => 'created_at desc'
                    ));
                }
                $numElements = count($documents);
                //riempio il json
                $json['numElements'] = $numElements;
                $json['max_page'] = ceil($numElements/5);
                $json['results'] = null;
                if(isset($documents) && $documents) {
                    foreach ( $documents as $document )
                    {
                        $result = null;
                        $result[]='<a href="'.$this->createLink($document->type,'view',$document->id).'">'.$document->title.'</a>';
                        if(isset($document->created_at) && is_a($document->created_at, 'ActiveRecord\DateTime'))
                        {
                            $result[]=$document->created_at->format('d/m/Y');
                        } 
                        else 
                        {
                            $result[]='-';
                        }
                        $json['results'][] = $result;
                    }
                } else {
                    $json['results'][0][0] = 'Nessun nuovo elemento';
                    $json['results'][0][1] = '';
                }
                header('ContentType: application/json');
                echo json_encode($json);
                break;
            case 'actsTable':
                $json = null;
                if(!$this->isUserLogged()) {
                    $documents = Document::find_all_by_type_and_status_and_edit_of('act',1,null, array(
                        'limit' => 5,
                        'offset' => 0,
                        'order' => 'created_at desc'
                    ));
                }
                else {
                    $documents = Document::find_all_by_type_and_edit_of('act',null, array(
                        'limit' => 5,
                        'offset' => 0,
                        'order' => 'created_at desc'
                    ));
                }
                $numElements = count($documents);
                //riempio il json
                $json['numElements'] = $numElements;
                $json['max_page'] = ceil($numElements/5);
                $json['results'] = null;
                if(isset($documents) && $documents) {
                    foreach ( $documents as $document )
                    {
                        $result = null;
                        $result[]='<a href="'.$this->createLink($document->type,'view',$document->id).'">'.$document->title.'</a>';
                        if(isset($document->created_at) && is_a($document->created_at, 'ActiveRecord\DateTime'))
                        {
                            $result[]=$document->created_at->format('d/m/Y');
                        } 
                        else 
                        {
                            $result[]='-';
                        }
                        $json['results'][] = $result;
                    }
                } else {
                    $json['results'][0][0] = 'Nessun nuovo elemento';
                    $json['results'][0][1] = '';
                }
                header('ContentType: application/json');
                echo json_encode($json);
                break;
            case 'articleTable':
                $json = null;
                if(!$this->isUserLogged()) {
                    $documents = Document::find_all_by_type_and_status_and_edit_of('article',1,null, array(
                        'limit' => 5,
                        'offset' => 0,
                        'order' => 'created_at desc'
                    ));
                }
                else {
                    $documents = Document::find_all_by_type_and_edit_of('article',null, array(
                        'limit' => 5,
                        'offset' => 0,
                        'order' => 'created_at desc'
                    ));
                }
                $numElements = count($documents);
                //riempio il json
                $json['numElements'] = $numElements;
                $json['max_page'] = ceil($numElements/5);
                $json['results'] = null;
                if(isset($documents) && $documents) {
                    foreach ( $documents as $document )
                    {
                        $result = null;
                        $result[]='<a href="'.$this->createLink($document->type,'view',$document->id).'">'.$document->title.'</a>';
                        if(isset($document->created_at) && is_a($document->created_at, 'ActiveRecord\DateTime'))
                        {
                            $result[]=$document->created_at->format('d/m/Y');
                        } 
                        else 
                        {
                            $result[]='-';
                        }
                        $json['results'][] = $result;
                    }
                } else {
                    $json['results'][0][0] = 'Nessun nuovo elemento';
                    $json['results'][0][1] = '';
                }
                header('ContentType: application/json');
                echo json_encode($json);
                break;
            case 'chapterTable':
                $json = null;
                if(!$this->isUserLogged()) {
                    $documents = Document::find_all_by_type_and_status_and_edit_of('chapter',1,null, array(
                        'limit' => 5,
                        'offset' => 0,
                        'order' => 'created_at desc'
                    ));
                }
                else {
                    $documents = Document::find_all_by_type_and_edit_of('chapter',null, array(
                        'limit' => 5,
                        'offset' => 0,
                        'order' => 'created_at desc'
                    ));
                }
                $numElements = count($documents);
                //riempio il json
                $json['numElements'] = $numElements;
                $json['max_page'] = ceil($numElements/5);
                $json['results'] = null;
                if(isset($documents) && $documents) {
                    foreach ( $documents as $document )
                    {
                        $result = null;
                        $result[]='<a href="'.$this->createLink($document->type,'view',$document->id).'">'.$document->title.'</a>';
                        $result[]=$document->info->name;
                        if(isset($document->created_at) && is_a($document->created_at, 'ActiveRecord\DateTime'))
                        {
                            $result[]=$document->created_at->format('d/m/Y');
                        } 
                        else 
                        {
                            $result[]='-';
                        }
                        $json['results'][] = $result;
                    }
                } else {
                    $json['results'][0][0] = 'Nessun nuovo elemento';
                    $json['results'][0][1] = '';
                    $json['results'][0][2] = '';
                }
                header('ContentType: application/json');
                echo json_encode($json);
                break;
            case 'booksTable':
                $json = null;
                if(!$this->isUserLogged()) {
                    $documents = Document::find_all_by_type_and_status_and_edit_of('book',1,null, array(
                        'limit' => 5,
                        'offset' => 0,
                        'order' => 'created_at desc'
                    ));
                }
                else {
                    $documents = Document::find_all_by_type_and_edit_of('book',null, array(
                        'limit' => 5,
                        'offset' => 0,
                        'order' => 'created_at desc'
                    ));
                }
                $numElements = count($documents);
                //riempio il json
                $json['numElements'] = $numElements;
                $json['max_page'] = ceil($numElements/5);
                $json['results'] = null;
                if(isset($documents) && $documents) {
                    foreach ( $documents as $document )
                    {
                        $result = null;
                        $result[]='<a href="'.$this->createLink($document->type,'view',$document->id).'">'.$document->title.'</a>';
                        if(isset($document->created_at) && is_a($document->created_at, 'ActiveRecord\DateTime'))
                        {
                            $result[]=$document->created_at->format('d/m/Y');
                        } 
                        else 
                        {
                            $result[]='-';
                        }
                        $json['results'][] = $result;
                    }
                } else {
                    $json['results'][0][0] = 'Nessun nuovo elemento';
                    $json['results'][0][1] = '';
                }
                header('ContentType: application/json');
                echo json_encode($json);
                break;
        }
    }
}

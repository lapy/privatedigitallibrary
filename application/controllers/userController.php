<?php

/**
 * Class userController
 *
 */
class userController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/user/index (which is the default page btw)
     */
    public function index($choice = 0)
    {
        if($this->isUserManager())
        {
            // load views
            require APP . 'views/_templates/header.php';
            require APP . 'views/users/index.php';
            require APP . 'views/_templates/pagination.php';
            require APP . 'views/_templates/footer.php';
        }
        else
        {
            // se l'utente non è manager, mostra l'errore restrictedAccess
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->restrictedAccess();
        }
    }

    public function insert()
    {
        if($this->isUserManager())
        {
            if (!empty($_POST))
            {
                $user = new User();
                $user->name = $_POST['name'];
                $user->surname = $_POST['surname'];
                $user->email = $_POST['email'];
                $user->verification_token = $this->generateRandomString();
                $user->activated = 0;
                $user->role = 1;
                
                $this->mail->addAddress($user->email, $user->name . ' ' . $user->surname);
                $this->mail->isHTML(true);
                $this->mail->Subject = 'Sei stato invitato a partecipare a PrivateDigitalLibrary';
                $this->mail->Body = '<p>Ciao ' . $user->name . ', <br />sei stato invitato a far parte di PrivateDigitalLibrary.<br />';
                $this->mail->Body .= 'Per attivare il tua account clicca sul link seguente<br /><br /></p>';
                $this->mail->Body .= '<a href="' . $this->createLink('auth', 'register', $user->verification_token) . '">';
                $this->mail->Body .= $this->createLink('auth', 'register', $user->verification_token) . '</a>';

                if(!$this->mail->send())
                {
                    $mailMessage = 'Impossibile inviare l\'email';
                    $mailMessage .= " Errore: " . $mail->ErrorInfo;
                }
                else
                {
                    $mailMessage = 'Utente inserito. Email di invito inviata a ' . $user->email;
                    try {
                        $user->save();
                    } catch (Exception $e) {
                        // database error
                        require APP . 'controllers/errorController.php';
                        $page = new errorController();
                        $page->dbError($e);
                        
                    }
                    
                }

                // load views.
                require APP . 'views/_templates/header.php';
                echo '<div class="contents"><p class="confirmation">'.$mailMessage.'. Per inserire un nuovo utente clicca <a href="'.$this->createLink('user','insert').'">qui</a>.</p></div>';
                require APP . 'views/_templates/footer.php';
            }
            else
            {
                // load views
                require APP . 'views/_templates/header.php';
                require APP . 'views/users/insert.php';
                require APP . 'views/_templates/footer.php';
            }
        }
        else
        {
            // se l'utente non è manager, mostra l'errore restrictedAccess
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->restrictedAccess();
        }
    }

    public function delete($userId)
    {
        $user = User::find($userId);
        $documents = $user->documents;
        foreach ($documents as $document)
        {
            $document->user_id = 2;
            
            try {
                $document->save();
            } catch (Exception $e) {
                // database error
                require APP . 'controllers/errorController.php';
                $page = new errorController();
                $page->dbError($e);
                
            }
            
        }
        Document_modification_token::table()->delete(array('user_id' => $userId ));
        
        try {
            $user->delete();
        } catch (Exception $e) {
            // database error
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->dbError($e);
            
        }
        
        header('Location: ' . $this->createLink('user', 'index'));
    }

    public function edit()
    {
        if ($this->isUserLogged())
        {
            if (!empty($_POST))
            {
                $user = $_SESSION['user'];
                $successful = false;
                if (isset($_POST['password']) && md5($_POST['password']) == $user->password)
                {
                    if ($_POST['email_edit'] != $user->email)
                    {
                        $user->email = $_POST['email_edit'];
                    }
                    if (isset($_POST['newpassword']) && $_POST['password'] != $_POST['newpassword'] && !empty($_POST['newpassword']))
                    {
                        $user->password = md5($_POST['newpassword']);
                    }
                    
                    try {
                        $user->save();
                    } catch (Exception $e) {
                        // database error
                        require APP . 'controllers/errorController.php';
                        $page = new errorController();
                        $page->dbError($e);
                        
                    }
                    

                    // Stampa una conferma di avvenuta modifica
                    require APP . 'views/_templates/header.php';
                    echo '<div class="contents"><p class="confirmation" >Le modifiche sono state effettuate.</p></div>';
                    require APP . 'views/_templates/footer.php';
                }
                else
                {
                    // Stampa errore in caso di password errata 
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->invalidPassword();
                }
            }
            else 
            {
                // Carica le viste necessarie alla compilazione del form
                require APP . 'views/_templates/header.php';
                require APP . 'views/users/edit.php';
                require APP . 'views/_templates/footer.php';
            }
        }
        else
        {
            // se l'utente non è loggato, mostra l'errore
            // restricted access: show the error page
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->restrictedAccess();
        }
    }

    public function profile($username = null)
    {
        if($this->isUserLogged())
        {            
            if($username)
            {
                try {
                    $user = User::find_by_username($username);
                } catch (Exception $e) {
                    // not found
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->index();
                }

                if(isset($user))
                {
                    // load views
                    require APP . 'views/_templates/header.php';
                    require APP . 'views/users/profile.php';
                    require APP . 'views/_templates/pagination.php';
                    require APP . 'views/_templates/footer.php';
                }
                else
                {
                    // not found
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->index();
                }
            }        
            else
            {
                //se non è specificato l'username mostra il proprio profilo
                $user = $_SESSION['user'];
                
                // load views
                require APP . 'views/_templates/header.php';
                require APP . 'views/users/profile.php';
                require APP . 'views/_templates/pagination.php';
                require APP . 'views/_templates/footer.php';
            }
        }
        else
        {
            // se l'utente non è loggato, mostra l'errore
            // restricted access: show the error page
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->restrictedAccess();
        }
    }

    public function pendingEdits()
    {
        if($this->isUserLogged())
        {
            require APP . 'views/_templates/header.php';
            require APP . 'views/users/pendingEdits.php';
            require APP . 'views/_templates/pagination.php';
            require APP . 'views/_templates/footer.php';
        }
        else
        {
            // se l'utente non è loggato, mostra l'errore
            // restricted access: show the error page
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->restrictedAccess();
        }
    }

    public function pendingEditsAjax($pageNumber = 1, $elementsPerPage = 2, $searchString = null)
    {
        if($this->isUserLogged())
        {
            $userId = $_SESSION['user']->id;
            $getmaxelements = isset($_GET['maxelements']) ? true : false;
            $json = null;
            $offset = ($pageNumber-1) * $elementsPerPage;
            
            //se non è una ricerca
            if($searchString == null)
            {
                if($userId)
                {
                    if($this->isUserManager())
                        $conditions = array('edit_of IS NOT ?', null);
                    else
                        $conditions = array('user_id = ? AND edit_of IS NOT ?', $userId, null);
    
                    $numElements = Document::find('all', array(
                       'select' => 'count(*) AS num_rows',
                       'conditions' => $conditions
                    ))[0]->num_rows;
                    if($getmaxelements) { echo json_encode($numElements); die(); }
                    $documents = Document::find('all', array(
                       'limit' => $elementsPerPage,
                       'offset' => $offset,
                       'conditions' => $conditions
                    ));
                }
            }
            //se è una ricerca
            else
            {
                $searchString = strtolower($searchString);
                $searchString = explode('-',$searchString);
                $query = '';
                foreach ($searchString as $key => $value) {
                    $query .= '(';
                    foreach(Document::connection()->columns('documents') as $attribute)
                    {
                        if($attribute->name != 'status')
                            $query .= '`' . $attribute->name . '`' . " LIKE '%".$value."%' OR ";
    
                    }
                    //elimino l'OR in eccesso
                    $query = substr($query, 0, -3);
                    $query .= ') AND ';
                }
                
                //elimino l'AND in eccesso
                $query = substr($query, 0, -4);
                
                if($userId)
                {
                    if($this->isUserManager())
                        $conditions = array($query.' AND user_id IN (?) AND edit_of IS NOT ?', array($userId, 2), null);
                    else
                        $conditions = array($query.' AND user_id = ? AND edit_of IS NOT ?', $userId, null);
    
                    $numElements = Document::find('all', array(
                        'select' => 'count(*) AS num_rows',
                        'conditions' => $conditions
                    ))[0]->num_rows;
                    if($getmaxelements) { echo json_encode($numElements); die(); }
                    $documents = Document::find('all', array(
                        'limit' => $elementsPerPage,
                        'offset' => $offset,
                        'conditions' => $conditions
                    ));
                }
                //echo Document::connection()->last_query;
            }
            //riempio il json
            $json['numElements'] = $numElements;
            $json['max_page'] = ceil($numElements/$elementsPerPage);
            foreach ( $documents as $document )
            {
                $result = null;
                $result[]='<a href="'.$this->createLink($document->type,'view',$document->id).'">'.$document->title.'</a>';
                $result[]=$document->author;
                $result[]=$document->year;
                switch($document->type)
                {
                    case 'book':
                        $result[]='Libro';
                        break;
                    case 'act':
                        $result[]='Atto';
                        break;
                    case 'chapter':
                        $result[]='Capitolo';
                        break;
                    case 'article':
                        $result[]='Articolo';
                        break;
                }
                if(isset($document->created_at) && is_a($document->created_at, 'ActiveRecord\DateTime'))
                {
                    $result[]=$document->created_at->format('d/m/Y');
                } 
                else 
                {
                    $result[]='-';
                }
                if($document->status == 0)
                    $result[]='Privata';
                else
                    $result[]='Pubblica';
                $json['results'][] = $result;
            }
            header('ContentType: application/json');
            echo json_encode($json);
        }
    }

    public function userAjax($pageNumber = 1, $elementsPerPage = 2, $searchString = null)
    {
        if($this->isUserManager())
        {
            $userType = isset($_GET['userType']) ? $_GET['userType'] : 0;

            switch ($userType) {
                case 0:
                    $userTypeQuery = 'id != 2';
                    break;
                case 1:
                    $userTypeQuery = 'id != 2 AND activated = 1';
                    break;
                case 2:
                    $userTypeQuery = 'id != 2 AND activated = 0';
                    break;
            }
            
            $json = null;
            $offset = ($pageNumber-1) * $elementsPerPage;
            $getmaxelements = isset($_GET['maxelements']) ? true : false;
            
            //se non è una ricerca
            if($searchString == null)
            {
                if($this->isUserManager()) 
                {
                    $numElements = User::find('all', array(
                        'select' => 'count(*) AS num_rows',
                        'conditions' => array('role = 1 AND ' . $userTypeQuery)
                    ))[0]->num_rows;
                    if($getmaxelements) { echo json_encode($numElements); die(); }
                    $users = User::find('all', array(
                        'limit' => $elementsPerPage,
                        'offset' => $offset,
                        'conditions' => array('role = 1 AND ' . $userTypeQuery)
                    ));
                }
            }
            //se è una ricerca
            else
            {
                $searchString = strtolower($searchString);
                $searchString = explode('-',$searchString);
                
                $query = '(';
                foreach ($searchString as $keyword)
                {
                    $query .= "username LIKE '%" . $keyword . "%' OR email LIKE '%" . $keyword . "%' AND ";
                }
                $query = substr($query, 0, -5);
                $query .= ') AND role = 1';
                $numElements = User::find('all', array(
                    'select' => 'count(*) AS num_rows',
                    'conditions' => array($query . ' AND ' . $userTypeQuery)
                ))[0]->num_rows;
                if($getmaxelements) { echo json_encode($numElements); die(); }
                $users = User::find('all', array(
                    'limit' => $elementsPerPage,
                    'offset' => $offset,
                    'conditions' => array($query . ' AND ' . $userTypeQuery)
                ));
                //echo User::connection()->last_query;
            }
            //riempio il json
            $json['numElements'] = $numElements;
            $json['max_page'] = ceil($numElements/$elementsPerPage);
            if ($numElements) {
                foreach ( $users as $user )
                {
                    $result = null;
                    if (!empty($user->username))
                        $result[]='<a href="'.$this->createLink('user','profile',$user->username).'">'.$user->username.'</a>';
                    else
                        $result[]='-';
                    $result[]=$user->email;                           
                    if(isset($user->created_at) && is_a($user->created_at, 'ActiveRecord\DateTime'))
                    {
                        $result[]=$user->created_at->format('d/m/Y H:i:s');
                    } 
                    else 
                    {
                        $result[]='-';
                    }
                    if(isset($user->updated_at) && is_a($user->updated_at, 'ActiveRecord\DateTime'))
                    {
                        $result[]=$user->updated_at->format('d/m/Y H:i:s');
                    } 
                    else 
                    {
                        $result[]='-';
                    }
                    $result[]='<a class="delete" id="' . $user->id . '" onclick="hookDelete(event, \'#' . $user->id . '\');" href="'.$this->createLink('user', 'delete', $user->id).'"><img src="' . URL . '/public/css/images/delete.png" alt="icona rimozione utente" /></a>';
                    $json['results'][] = $result;
                }
            }
            else
            {
                $json['results'][0][0] = 'Nessun utente trovato';
                $json['results'][0][1] = '-';
                $json['results'][0][2] = '-';
                $json['results'][0][3] = '-';
                $json['results'][0][4] = '-';
                $json['numElements'] = 1;
                $json['max_page'] = 1;
            }
            header('ContentType: application/json');
            echo json_encode($json);
        }
    }
}
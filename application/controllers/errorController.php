<?php

/**
 * Class ErrorController
 *
 */
class errorController extends Controller
{
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        header("HTTP/1.0 404 Not Found");
        // load views
        require APP . 'views/_templates/header.php';
        require APP . 'views/error/index.php';
        require APP . 'views/_templates/footer.php';
        die();
    }

    public function restrictedAccess()
    {
        // load views
        require APP . 'views/_templates/header.php';
        require APP . 'views/error/restrictedAccess.php';
        require APP . 'views/_templates/footer.php';
        die();
    }

    public function invalidToken()
    {
        // load views
        require APP . 'views/_templates/header.php';
        echo '<div class="contents"><h1 class="title">Errore</h1><p class="warning">Non sei autorizzato a vedere questa pagina. Per registrarti devi essere invitato dal responsabile.</p></div>';
        require APP . 'views/_templates/footer.php';
        die();
    }

    public function alreadyRegistered()
    {
        // load views
        require APP . 'views/_templates/header.php';
        echo '<div class="contents"><h1 class="title">Errore</h1><p class="warning">Attualmente sei connesso con un account già registrato.</p></div>';
        require APP . 'views/_templates/footer.php';
        die();
    }

    public function invalidPassword()
    {
        // load views
        require APP . 'views/_templates/header.php';
        echo '<p class="warning" >Il campo password attuale non corrisponde alla password presente nel database, ricompila il form.</p>';
        require APP . 'views/users/edit.php';
        require APP . 'views/_templates/footer.php';
        die();
    }

    public function notExists()
    {
        // load views
        require APP . 'views/_templates/header.php';
        echo '<p class="warning" >Il nome utente o l\'email non esistono nel database.</p>';
        require APP . 'views/auth/lostPassword1.php';
        require APP . 'views/_templates/footer.php';
        die();
    }

    public function dbError($exception)
    {
        // load views
        require APP . 'views/_templates/header.php';
        echo '<div class="contents"><h1 class="title">Errore Irrecuperabile</h1><p class="warning" >Si è verificato un errore col database. Prego contattare l\'amministratore.<br />';
        echo 'Dettagli errore: '. $exception->getMessage() .'</p></div>';
        require APP . 'views/_templates/footer.php';
        die();
    }
}

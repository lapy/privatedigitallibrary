<?php

/**
 * Class authController
 *
 */
class authController extends Controller
{
	/**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/auth/index
     */
    public function index()
    {
        // load views. 
        require APP . 'views/_templates/header.php';
        require APP . 'views/login/index.php';
        require APP . 'views/_templates/footer.php';
    }

    /**
     * PAGE: login
     * This method handles what happens when you move to http://yourproject/auth/login
     */
    public function login()
    {
    	if(isset($_POST['username']) && isset($_POST['password']))
        {
            $username = $_POST['username'];
            $password = $_POST['password'];
            if($this->authenticate($username, $password)) 
            {
                header('Location: ' . $_POST['redirect']);
                return true;
            }
                
            else 
            {
                // load views.
                require APP . 'views/_templates/header.php';
                echo '<p class="warning">Errore: Il nome utente o la password sono errati</p>';
                require APP . 'views/auth/login.php';
                require APP . 'views/_templates/footer.php';
            }
        }
        else
        {
            // load views.
            require APP . 'views/_templates/header.php';
            require APP . 'views/auth/login.php';
            require APP . 'views/_templates/footer.php';
        }
            
    }

    /**
     * METHOD: authenticate
     * This method handles authentication but has no view
     */
    public function authenticate($username, $password)
    {

        $user = User::find_by_username($username);
        if (isset($user->id) && $user->password == md5($password) && $user->activated == 1)
        {
            $_SESSION['user'] = $user;
            return true;
        }
        else
        {
            return false;  
        } 
    }

    /**
     * METHOD: logout
     * This method handles logout but has no view
     */
    public function logout()
    {
        header('Location: ' . URL);
        session_destroy();
    }

    /**
     * PAGE: register
     * This method handles what happens when you move to http://yourproject/auth/register
     */
    public function register($token = null)
    {
        $user = User::find_by_verification_token($token);
        $tokenexists = isset($user->id) ? true : false;
        $successful = null;  


        if (!$this->isUserLogged())
        {
            if ($tokenexists)
            {
                if(!empty($_POST))
                {
                    $user->username = $_POST['username'];
                    $user->password = md5($_POST['password']);
                    $user->verification_token = null;
                    $user->activated = 1;
                    $user->role = 1;
                    
                    try {
                        $user->save();
                    } catch (Exception $e) {
                        // database error
                        require APP . 'controllers/errorController.php';
                        $page = new errorController();
                        $page->dbError($e);
                        
                    }
    
                    // load views.
                    require APP . 'views/_templates/header.php';
                    echo '<div class="contents"><p class="confirmation">Il tuo account è ora attivo! Effettua il login per accedere a tutti i contenuti.</p></div>';
                    require APP . 'views/_templates/footer.php';
                }
                else
                    {
                        // load views.
                        require APP . 'views/_templates/header.php';
                        require APP . 'views/auth/register.php';
                        require APP . 'views/_templates/footer.php';
                    }
            }
            else
            {
                // se il token non esiste, mostra l'errore invalidToken
                require APP . 'controllers/errorController.php';
                $page = new errorController();
                $page->invalidToken();
            }
        }
        else
        {
            // se l'utente è loggato, mostra l'errore alreadyRegistered
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->alreadyRegistered();
        }
    }

    public function lostPassword($token = null)
    {
        if (!$this->isUserLogged())
        {
            //se nel link non c'è il token
            if (!$token)
            {
                //se la form lostPassword1 è stata inviata, elaborala
                if(!empty($_POST))
                {
                    if(isset($_POST['username']))
                    {
                        $user = User::find_by_username_and_email($_POST['username'], $_POST['email']);
        
                        //se l'utente esiste ed è attivato crea un token e invia l'email col link di password dimenticata
                        if(isset($user) && $user->activated == 1)
                        {
                            $user->verification_token = $this->generateRandomString();
                            
                            try {
                                $user->save();
                            } catch (Exception $e) {
                                // database error
                                require APP . 'controllers/errorController.php';
                                $page = new errorController();
                                $page->dbError($e);
                                
                            }
        
                            $this->mail->addAddress($user->email, $user->name . ' ' . $user->surname);
                            $this->mail->isHTML(true);
                            $this->mail->Subject = 'Password Smarrita';
                            $this->mail->Body = '<p>Ciao ' . $user->name . ', <br />ci &egrave; arrivata una richiesta di recupero password smarrita riguardo il tuo account PrivateDigitalLibrary.<br />';
                            $this->mail->Body .= 'Per per resettare la tua password clicca sul link seguente<br /><br /></p>';
                            $this->mail->Body .= '<a href="' . $this->createLink('auth', 'lostPassword', $user->verification_token) . '">';
                            $this->mail->Body .= $this->createLink('auth', 'lostPassword', $user->verification_token) . '</a>';
                            $this->mail->Body .= '<p>Se non sei stato tu, ignora semplicemente questa email.</p>';
        
                            if(!$this->mail->send())
                            {
                                $this->mailMessage = 'Il messaggio non è stato inviato';
                                $this->mailMessage .= " Errore: " . $this->mail->ErrorInfo;
                            }
                            else
                                $this->mailMessage = 'Richiesta inviata all\' email ' . $user->email . '. Se non visualizzi l\'email controlla nella cartella di spam.';
                            
                            //mostra conferma o errore
                            // load views.
                            require APP . 'views/_templates/header.php';
                            echo '<div class="contents"><p class="confirmation">'.$this->mailMessage.'</p></div>';
                            require APP . 'views/_templates/footer.php';
                        }
                        else
                        {
                            // se l'utente è loggato, mostra l'errore notExists
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->notExists();
                        }
                    }
                } 
                //se la form lostPassword1 non è stata inviata, mostrala
                else
                {
                    // load views.
                    require APP . 'views/_templates/header.php';
                    require APP . 'views/auth/lostPassword1.php';
                    require APP . 'views/_templates/footer.php';
                }
            }
            //caso in cui nel link c'è il token
            else
            {
                $user = User::find_by_verification_token($token);
                //se trovo l'utente
                if(isset($user->id))
                {
                    //se la form lostPassword2 è stata inviata, elaborala
                    //(caso invio password dalla form lostPassword2)
                    if(!empty($_POST))
                    {
                        if(isset($_POST['password-lost']))
                        {
                            $user->password = md5($_POST['password-lost']);
                            $user->save();
                            // load views.
                            require APP . 'views/_templates/header.php';
                            echo '<div class="contents"><p class="confirmation">La tua password è stata reimpostata con successo.</p></div>';
                            require APP . 'views/_templates/footer.php';
                        }
                        else
                        {
                            // load views.
                            require APP . 'views/_templates/header.php';
                            echo '<div class="contents"><p class="warning">La password non può essere vuota.</p></div>';
                            require APP . 'views/auth/lostPassword2.php';
                            require APP . 'views/_templates/footer.php'; 
                        }
                    }
                    //se la form lostPassword2 non è stata inviata, mostrala
                    //(caso apertura link password smarrita dall'email)
                    else
                    {
                        // load views.
                        require APP . 'views/_templates/header.php';
                        require APP . 'views/auth/lostPassword2.php';
                        require APP . 'views/_templates/footer.php';   
                    }
                }
            }
        }
        else
        {
            // se l'utente è loggato, mostra l'errore alreadyRegistered
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->alreadyRegistered();
        }
    }
}
<?php

/**
 * Class documentController
 *
 */
class documentController extends Controller
{
	/**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/document/index
     */
    public function index()
    {      
        // load views. within the views we can echo out $documenti
        require APP . 'views/_templates/header.php';
        require APP . 'views/documents/index.php';
        require APP . 'views/_templates/pagination.php';
        require APP . 'views/_templates/footer.php';
    }

    public function getXml($id)
    {
        // se loggato mostra il form
        if($this->isUserLogged())
        {
            try
            {
                $document = Document::find($id);
                
                $xml = $document->to_XML(array(
                    'include' => array($document->type),
                    'except' => array('edit_of', 'user_id')
                ));
                header("Pragma: public"); // required
                header("Expires: 0"); 
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
                header("Cache-Control: private",false); // required for certain browsers 
                header("Content-type: text/xml");
                header('Content-Disposition: attachment; filename="'.urlencode($document->title).'.xml"');
                $xml = substr_replace($xml, '<!DOCTYPE document SYSTEM "'.URL.'/document.dtd">', 38, 0);
                echo $xml;
            }
            catch (Exception $e)
            {
                die();
            }
        }
    }

    public function create()
    {
        // se loggato mostra il form
        if($this->isUserLogged())
        {
            // se il form è stato inviato, elaboralo
            if (!empty($_POST))
            {
                $document = new Document();
                $document->user_id = $_SESSION['user']->id;
                $document->comment = $_POST['comment'];
                $document->status = $_POST['status'];
                $document->author = $_POST['author'];
                $document->title = $_POST['title'];
                $document->year = $_POST['year'];
                $document->url = $_POST['url'];
                $document->type = $_POST['type'];
                
                try {
                    $document->save();
                } catch (Exception $e) {
                    // database error
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->dbError($e);
                    
                }

                if(isset($_POST['tags']))
                foreach ($_POST['tags'] as $key => $value) {
                    Has_tag::create( array(
                        
                            'document_id' => $document->id,
                            'tag_id' => $value
                    ));
                }

                switch($_POST['type'])
                {
                    case 'act':
                        $act = new Act();
                        $act->document_id = $document->id;
                        $act->name = $_POST['act_name'];
                        $act->location = $_POST['act_location'];
                        $act->date = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['act_date'])));
                        $act->start_page = $_POST['act_start_page'];
                        $act->end_page = $_POST['act_end_page'];
                        try {
                            $act->save();
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);
                            
                        }
                        
                        break;

                    case 'article':
                        $article = new Article();
                        $article->document_id = $document->id;
                        $article->name = $_POST['article_name'];
                        $article->volume = $_POST['article_volume'];
                        $article->issue = $_POST['article_issue'];
                        $article->start_page = $_POST['article_start_page'];
                        $article->end_page = $_POST['article_end_page'];
                        try {
                            $article->save();
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);
                            
                        }
                        break;

                    case 'book':
                        $book = new Book();
                        $book->document_id = $document->id;
                        $book->editor = $_POST['book_editor'];
                        $book->edition = $_POST['book_edition'];
                        try {
                            $book->save();
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);
                            
                        }
                        break;

                    case 'chapter':
                        $chapter = new Chapter();
                        $chapter->document_id = $document->id;
                        $chapter->curators = $_POST['chapter_curators'];
                        $chapter->name = $_POST['chapter_name'];
                        $chapter->editor = $_POST['chapter_editor'];
                        $chapter->start_page = $_POST['chapter_start_page'];
                        $chapter->end_page = $_POST['chapter_end_page'];
                        try {
                            $chapter->save();
                        } catch (Exception $e) {
                            // database error
                            require APP . 'controllers/errorController.php';
                            $page = new errorController();
                            $page->dbError($e);
                            
                        }
                        break;
                }
                // load views.
                require APP . 'views/_templates/header.php';
                echo '<div class="contents"><p class="confirmation">Documento inserito! Sarai redirezionato ai dettagli fra 5 secondi.</p></div>';
                echo "<script type=\"text/javascript\">setTimeout(function () {window.location.href='".$this->createLink($document->type, 'view', $document->id)."';},5000);</script>";
                require APP . 'views/_templates/footer.php';
            } else {
                // se il form non è stato inviato
                // load views.
                require APP . 'views/_templates/header.php';
                require APP . 'views/documents/create.php';
                require APP . 'views/_templates/footer.php';
            }
        }
        else
        {
            // se l'utente non è loggato, mostra l'errore
            // restricted access: show the error page
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->restrictedAccess();
        }
    }

    public function delete($id)
    {
        try
        {
            $document = Document::find($id);

            try {
                $document->delete();
            } catch (Exception $e) {
                // database error
                require APP . 'controllers/errorController.php';
                $page = new errorController();
                $page->dbError($e);
                
            }
                
            
            require APP . 'views/_templates/header.php';
            echo '<div class="contents"><p class="confirmation">Il documento è stato eliminato.</p></div>'; 
            require APP . 'views/_templates/footer.php';
        } 
        catch (Exception $e)
        {
            // not found: show the error page
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->index();
            
        }
    }

    public function editAccept($usertype,$token)
    {
        $approved = false;
        switch($usertype)
        {
            case 'user':
                if($edit = Document_modification_token::find_by_user_token($token))
                {
                    //se l'utente che ha modificato il documento non è un manager, approva con riserva
                    if(User::find($edit->edit_user_id)->role != 0)
                    {
                        $edit->user_token = null;
                        $edit->save();
                        if($edit->admin_token == null) $approved = true;
                        $message = '<div id="contents"><p class="confirmation">Hai accettato le modifiche';
                        if(!$approved)
                        {
                            $message .= ', ma il responsabile non le ha ancora accettate, pertanto non sono state ancora applicate.';
                        }
                        else
                        {
                            $message .= ' e il responsabile le ha accettate prima di te, le modifiche sono state applicate.';
                        }
                        $message .= '</p></div>';
                    }
                    //se l'utente che ha modificato il documento è un manager approva le modifiche direttamente
                    else
                    {
                        $edit->user_token = null;
                        $edit->admin_token = null;
                        $edit->save();
                        $approved = true;
                        $message = '<div id="contents"><p class="confirmation">Le modifiche sono state applicate.</p></div>';
                    }
                }
                else
                {
                    // not found: show the error page
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->index();
                }
                break;
            case 'manager':
                if($edit = Document_modification_token::find_by_admin_token($token))
                {
                    //se l'utente che ha originariamente inserito il documento non è questo manager e non è sconosciuto, approva con riserva
                    if(Document::find($edit->document_id)->user_id != $_SESSION['user']->id && Document::find($edit->document_id)->user_id != 2)
                    {
                        $edit->admin_token = null;
                        $edit->save();
                        if($edit->user_token == null) $approved = true;
                        $message = '<div id="contents"><p class="confirmation">Hai accettato le modifiche';
                        if(!$approved)
                        {
                            $message .= ', ma il l\'utente inseritore non le ha ancora accettate, pertanto non sono state ancora applicate.';
                        }
                        else
                        {
                            $message .= ' e l\'utente inseritore le ha accettate prima di te, le modifiche sono state applicate.';
                        }
                        $message .= '</p></div>';
                    }
                    //se l'utente che ha originariamente inserito il documento è proprio questo manager o è l'utente sconosciuto approva le modifiche direttamente
                    else
                    {
                        $edit->admin_token = null;
                        $edit->user_token = null;
                        $edit->save();
                        $approved = true;
                        $message = '<div id="contents"><p class="confirmation">Le modifiche sono state applicate.</p></div>';
                    }
                }
                else
                {
                    // not found: show the error page
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->index();
                }
                break;
        }

        if($approved && $originalDocument = Document::find($edit->document_id))
        {
            if($editedDocument = Document::find_by_edit_of($originalDocument->id))
            {
                //applica le modifiche proposte
                $originalId = $originalDocument->id;
                $originalDate = $originalDocument->created_at;
                $newId = $editedDocument->id;
                
                $originalDocument->delete();
                
                $update = array('id' => $originalId);
                Document::table()->update($update, array('id' => $newId));

                $doc = Document::find($originalId);
                $doc->created_at = $originalDate;
                $doc->user_id = $edit->user_id;
                $doc->save();

                if(count($originalDocument->modification_tokens) > 1)
                    $originalDocument->modification_tokens[0]->delete();

                $this->updatePendingEdits();
            }
        }
        require APP . 'views/_templates/header.php';
        echo $message;
        require APP . 'views/_templates/footer.php';
    }

    public function editRefuse($usertype,$token)
    {
        $refused = false;
        switch($usertype)
        {
            case 'user':
                if($edit = Document_modification_token::find_by_user_token($token))
                {
                    $edit->user_token = null;
                    $edit->admin_token = null;
                    $refused = true;
                    
                }
                else
                {
                    // not found: show the error page
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->index();
                }
                break;
            case 'manager':
                if($edit = Document_modification_token::find_by_admin_token($token))
                {
                    $edit->admin_token = null;
                    $edit->user_token = null;
                    $refused = true;
                }
                else
                {
                    // not found: show the error page
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->index();
                }
                break;
        }
        //cancella le modifiche
        if($refused && $originalDocument = Document::find($edit->document_id))
        {
            if($editedDocument = Document::find_by_edit_of($originalDocument->id))
            {
                end($originalDocument->modification_tokens)->delete();
                $editedDocument->delete();
                $edit->delete();
                $this->updatePendingEdits();

                require APP . 'views/_templates/header.php';
                echo '<div id="contents"><p class="confirmation">Le modifiche ';
                echo 'sono state ignorate.';
                echo '</p></div>';
                require APP . 'views/_templates/footer.php';
            }
        }
    }

    public function tag($tagName = null)
    {
        if($tagName)
        {
            $tag = Tag::find_by_name($tagName);

            if($tag) {
                require APP . 'views/_templates/header.php';
                require APP . 'views/documents/tag.php';
                require APP . 'views/_templates/pagination.php';
                require APP . 'views/_templates/footer.php';
            } else {
                // not found: show the error page
                require APP . 'controllers/errorController.php';
                $page = new errorController();
                $page->index();
            }
        }
        else
        {
            // not found: show the error page
            require APP . 'controllers/errorController.php';
            $page = new errorController();
            $page->index();
        }
    }

    public function ajax($pageNumber = 1, $elementsPerPage = 2, $searchString = null)
    {
        $userId = isset($_GET['userId']) ? $_GET['userId'] : null;
        $getmaxelements = isset($_GET['maxelements']) ? true : false;
        $json = null;
        $offset = ($pageNumber-1) * $elementsPerPage;
        
        //se non è una ricerca
        if($searchString == null)
        {
            if(!$userId)
            {     
               if(!$this->isUserLogged()) {
                    $numElements = Document::find('all', array(
                       'select' => 'count(*) AS num_rows',
                       'conditions' => array('status = ? AND edit_of IS ?', 1, null)
                    ))[0]->num_rows;
                    if($getmaxelements) { echo json_encode($numElements); die(); }
                    $documents = Document::find('all', array(
                       'limit' => $elementsPerPage,
                       'offset' => $offset,
                       'conditions' => array('status = ? AND edit_of IS ?', 1, null)
                    ));
                }
                else
                {
                    $numElements = Document::find('all', array(
                       'select' => 'count(*) AS num_rows',
                       'conditions' => array('edit_of IS ?', null)
                    ))[0]->num_rows;
                    if($getmaxelements) { echo json_encode($numElements); die(); }
                    $documents = Document::find('all', array(
                       'limit' => $elementsPerPage,
                       'offset' => $offset,
                       'conditions' => array('edit_of IS ?', null)
                    ));
                }
            }
            else
            {
                if(!$this->isUserLogged()) {
                    $numElements = Document::find('all', array(
                       'select' => 'count(*) AS num_rows',
                       'conditions' => array('status = ? AND user_id = ? AND edit_of IS ?', 1, $userId, null)
                    ))[0]->num_rows;
                    if($getmaxelements) { echo json_encode($numElements); die(); }
                    $documents = Document::find('all', array(
                       'limit' => $elementsPerPage,
                       'offset' => $offset,
                       'conditions' => array('status = ? AND user_id = ? AND edit_of IS ?', 1, $userId, null)
                    ));
               }
               else
               {
                    $numElements = Document::find('all', array(
                       'select' => 'count(*) AS num_rows',
                       'conditions' => array('user_id = ? AND edit_of IS ?', $userId, null)
                    ))[0]->num_rows;
                    if($getmaxelements) { echo json_encode($numElements); die(); }
                    $documents = Document::find('all', array(
                       'limit' => $elementsPerPage,
                       'offset' => $offset,
                       'conditions' => array('user_id = ? AND edit_of IS ?', $userId, null)
                    ));
                }
                //echo Document::connection()->last_query;
            }
        }
        //se è una ricerca
        else
        {
            $searchString = strtolower($searchString);
            $searchString = explode('-',$searchString);
            $query = '';
            foreach ($searchString as $key => $value) {
                $query .= '(';
                foreach(Document::connection()->columns('documents') as $attribute)
                {
                    if($attribute->name != 'status')
                        $query .= '`' . $attribute->name . '`' . " LIKE '%".$value."%' OR ";

                }
                //elimino l'OR in eccesso
                $query = substr($query, 0, -3);
                $query .= ') AND ';
            }
            
            if(!$this->isUserLogged())
                //solo documenti pubblici
                $query .= '`status` = 1';
            else
                //elimino l'AND in eccesso
                $query = substr($query, 0, -4);
            
            if(!$userId)
            {        
                $numElements = Document::find('all', array(
                    'select' => 'count(*) AS num_rows',
                    'conditions' => array($query . ' AND edit_of IS ?', null)
                ))[0]->num_rows;
                if($getmaxelements) { echo json_encode($numElements); die(); }
                $documents = Document::find('all', array(
                    'limit' => $elementsPerPage,
                    'offset' => $offset,
                    'conditions' => array($query . ' AND edit_of IS ?', null),
                ));
            }
            else
            {
                $numElements = Document::find('all', array(
                    'select' => 'count(*) AS num_rows',
                    'conditions' => array($query.' AND user_id = ? AND edit_of IS ?', $userId, null)
                ))[0]->num_rows;
                if($getmaxelements) { echo json_encode($numElements); die(); }
                $documents = Document::find('all', array(
                    'limit' => $elementsPerPage,
                    'offset' => $offset,
                    'conditions' => array($query.' AND user_id = ? AND edit_of IS ?', $userId, null)
                ));
            }
            //echo Document::connection()->last_query;
        }
        //riempio il json
        $json['numElements'] = $numElements;
        $json['max_page'] = ceil($numElements/$elementsPerPage);
        foreach ( $documents as $document )
        {
            $result = null;
            $result[]='<a href="'.$this->createLink($document->type,'view',$document->id).'">'.$document->title.'</a>';
            $result[]=$document->author;
            $result[]=$document->year;
            switch($document->type)
            {
                case 'book':
                    $result[]='Libro';
                    break;
                case 'act':
                    $result[]='Atto';
                    break;
                case 'chapter':
                    $result[]='Capitolo';
                    break;
                case 'article':
                    $result[]='Articolo';
                    break;
            }
            if(isset($document->created_at) && is_a($document->created_at, 'ActiveRecord\DateTime'))
            {
                $result[]=$document->created_at->format('d/m/Y');
            } 
            else 
            {
                $result[]='-';
            }
            if($document->status == 0)
                $result[]='Privata';
            else
                $result[]='Pubblica';
            $json['results'][] = $result;
        }
        header('ContentType: application/json');
        echo json_encode($json);
    }

    public function tagAjax($pageNumber = 1, $elementsPerPage = 2, $searchString = null)
    {
        $getmaxelements = isset($_GET['maxelements']) ? true : false;
        $json = null;
        $offset = ($pageNumber-1) * $elementsPerPage;
        $tagName = isset($_GET['tagName']) ? $_GET['tagName'] : null;
        
        //se non è una ricerca
        if($searchString == null)
        {
            if($tagName)
            {     
               if(!$this->isUserLogged()) {
                    $tag = Tag::find_by_name($tagName);
                    $documents = $tag->public_documents;
                    $numElements = count($documents);
                    if($getmaxelements) { echo json_encode($numElements); die(); }
                }
                else
                {
                    $tag = Tag::find_by_name($tagName);
                    $documents = $tag->documents;
                    $numElements = count($documents);
                    if($getmaxelements) { echo json_encode($numElements); die(); }
                }
                //riempio il json
                $json['numElements'] = $numElements;
                $json['max_page'] = ceil($numElements/$elementsPerPage);
                foreach ( $documents as $document )
                {
                    $result = null;
                    $result[]='<a href="'.$this->createLink($document->type,'view',$document->id).'">'.$document->title.'</a>';
                    $result[]=$document->author;
                    $result[]=$document->year;
                    switch($document->type)
                    {
                        case 'book':
                            $result[]='Libro';
                            break;
                        case 'act':
                            $result[]='Atto';
                            break;
                        case 'chapter':
                            $result[]='Capitolo';
                            break;
                        case 'article':
                            $result[]='Articolo';
                            break;
                    }
                    if(isset($document->created_at) && is_a($document->created_at, 'ActiveRecord\DateTime'))
                    {
                        $result[]=$document->created_at->format('d/m/Y');
                    } 
                    else 
                    {
                        $result[]='-';
                    }
                    if($document->status == 0)
                        $result[]='Privata';
                    else
                        $result[]='Pubblica';
                    $json['results'][] = $result;
                }
                header('ContentType: application/json');
                echo json_encode($json);
            }
            else
            {
                $json['numElements'] = 1;
                $json['max_page'] = 1;
                $json['results'][0][0] = 'Nessun elemento';
                echo json_encode($json);
            }
        }
        
    }
}


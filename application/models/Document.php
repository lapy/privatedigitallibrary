<?php

class Document extends ActiveRecord\Model
{
    private $infoObj;

	static $belongs_to = array(
		array('user'),
	);

    static $has_many = array(
        array('has_tags', 'class_name' => 'Has_tag'),
        array('tags', 'through' => 'has_tags'),
        array('modification_tokens', 'class_name' => 'Document_modification_token'),
    );

    static $has_one = array(
        array('edit_user', 'class_name' => 'Edit_user', 'through' => 'document_modification_tokens'),
        array('act'),
        array('article'),
        array('book'),
        array('chapter')
    );

    public function get_Info()
    {
        if(!isset($this->infoObj)) {
            switch($this->type)
            {
                case 'book':
                try {
                    $this->infoObj = Book::find($this->id);
                } catch (Exception $e) {
                    // database error
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->dbError($e);
                    die();
                }
                
                break;

                case 'act':
                try {
                    $this->infoObj = Act::find($this->id);
                } catch (Exception $e) {
                    // database error
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->dbError($e);
                    die();
                }
                
                break;

                case 'article':
                try {
                    $this->infoObj = Article::find($this->id);
                } catch (Exception $e) {
                    // database error
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->dbError($e);
                    die();
                }
                
                break;

                case 'chapter':
                try {
                    $this->infoObj = Chapter::find($this->id);
                } catch (Exception $e) {
                    // database error
                    require APP . 'controllers/errorController.php';
                    $page = new errorController();
                    $page->dbError($e);
                    die();
                }
                
                break;
            }
        }
        return $this->infoObj;
    }
}
<?php 

class Document_modification_token extends ActiveRecord\Model
{
	static $belongs_to = array(
		array('document'),
		array('user'),
		array('edit_user', 'class_name' => 'Edit_user'),
	);
} 
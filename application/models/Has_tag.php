<?php 

class Has_tag extends ActiveRecord\Model
{
	static $belongs_to = array(
		array('document'),
		array('tag')
	);
} 
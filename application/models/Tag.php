<?php 

class Tag extends ActiveRecord\Model
{
	static $has_many = array(
        array('has_tags', 'class_name' => 'Has_tag'),
        array('documents', 'conditions' => array('edit_of IS ?', NULL), 'through' => 'has_tags'),
        array('public_documents', 'class_name' => 'Document', 'conditions' => array('status = ? AND edit_of IS ?', 1, NULL), 'through' => 'has_tags'),
    );
} 
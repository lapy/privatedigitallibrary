<?php 

class User extends ActiveRecord\Model
{
	static $has_many = array(
        array('document_modification_tokens', 'class_name' => 'Document_modification_token')
    );
}

class Edit_user extends ActiveRecord\Model
{
	static $table_name = 'users';
	
	static $has_many = array(
        array('document_modification_tokens', 'class_name' => 'Document_modification_token')
    );
} 
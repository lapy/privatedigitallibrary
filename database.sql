-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Gen 06, 2015 alle 23:05
-- Versione del server: 5.5.40
-- Versione PHP: 5.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pdldb`
--
DROP DATABASE IF EXISTS `pdldb`;
CREATE DATABASE IF NOT EXISTS `pdldb` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `pdldb`;

-- --------------------------------------------------------

--
-- Struttura della tabella `acts`
--

DROP TABLE IF EXISTS `acts`;
CREATE TABLE IF NOT EXISTS `acts` (
  `document_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `start_page` varchar(255) DEFAULT NULL,
  `end_page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `acts`
--

INSERT INTO `acts` (`document_id`, `name`, `location`, `date`, `start_page`, `end_page`) VALUES
(1, 'IEEE Conference 1998', 'California', '1998-07-16', '4', '6'),
(11, 'Identification, tracking and augmented reality for multiple robots equipped with coded blinking leds', 'Milano', '2014-07-23', '1', '1'),
(15, 'tatic Type Determination for C++', 'Baltimora', '1994-01-20', '1', '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `document_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `volume` varchar(255) DEFAULT NULL,
  `issue` varchar(255) DEFAULT NULL,
  `start_page` varchar(255) DEFAULT NULL,
  `end_page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `articles`
--

INSERT INTO `articles` (`document_id`, `name`, `volume`, `issue`, `start_page`, `end_page`) VALUES
(2, 'Ajax Today', '1', '23', '1', '5'),
(12, 'UniMc', '', '', '22', '28'),
(16, 'bibar.unisi.it', '', '', '31', '64'),
(17, 'Sistemi intelligenti', '1', '1', '85', '94'),
(22, 'UniMc', '1', '1', '22', '28');

-- --------------------------------------------------------

--
-- Struttura della tabella `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
  `document_id` int(10) unsigned NOT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `edition` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `books`
--

INSERT INTO `books` (`document_id`, `editor`, `edition`) VALUES
(4, 'Athena', '4'),
(5, 'Hops Libri', '1'),
(10, 'Per Brinch Hansen', '1'),
(13, 'Università degli studi di Ferrara', '2'),
(14, 'Nuova Biblioteca Dedalo', '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `chapters`
--

DROP TABLE IF EXISTS `chapters`;
CREATE TABLE IF NOT EXISTS `chapters` (
  `document_id` int(10) unsigned NOT NULL,
  `curators` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `start_page` varchar(255) DEFAULT NULL,
  `end_page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `chapters`
--

INSERT INTO `chapters` (`document_id`, `curators`, `name`, `editor`, `start_page`, `end_page`) VALUES
(3, 'Richard M. Stallman', 'Using and Porting the GNU Compiler Collection', 'Free Software Foundation', '5', '6'),
(6, 'Stallman, Lessing, Gay', 'Free Software, Free Society', 'Free Software Foundation', '13', '26'),
(9, 'Edseger W. Dijkstra', 'A discipline of programming', 'Prentice Hall, Inc.', '129', '134');

-- --------------------------------------------------------

--
-- Struttura della tabella `document_modification_tokens`
--

DROP TABLE IF EXISTS `document_modification_tokens`;
CREATE TABLE IF NOT EXISTS `document_modification_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `document_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `edit_user_id` int(10) unsigned NOT NULL DEFAULT '1',
  `admin_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_document` (`document_id`),
  KEY `fk_user` (`user_id`),
  KEY `fk_edit_user` (`edit_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dump dei dati per la tabella `document_modification_tokens`
--

INSERT INTO `document_modification_tokens` (`id`, `document_id`, `user_id`, `edit_user_id`, `admin_token`, `user_token`, `created_at`) VALUES
(3, 6, 4, 5, NULL, NULL, '2015-01-06 18:08:06'),
(5, 3, 4, 1, NULL, NULL, '2015-01-06 18:47:13'),
(6, 4, 2, 3, NULL, NULL, '2015-01-06 19:07:12'),
(7, 12, 5, 1, 'KhYV1PniAP', 'HUUS7CIUqu', '2015-01-06 23:03:56');

-- --------------------------------------------------------

--
-- Struttura della tabella `documents`
--

DROP TABLE IF EXISTS `documents`;
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0 Private, 1 Public',
  `author` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `year` varchar(4) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` enum('act','chapter','article','book') NOT NULL DEFAULT 'book',
  `edit_of` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documents` (`user_id`),
  KEY `fk_editof` (`edit_of`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dump dei dati per la tabella `documents`
--

INSERT INTO `documents` (`id`, `user_id`, `comment`, `status`, `author`, `title`, `year`, `url`, `type`, `edit_of`, `created_at`, `updated_at`) VALUES
(1, 3, 'Durata: 7 ore', 1, 'D.M Gavrila', 'The Visual Analysis of Human Movement: A Survey', '1998', 'http://www.sciencedirect.com/science/article/pii/S1077314298907160', 'act', NULL, '2015-01-02 18:14:47', '2015-01-06 19:16:41'),
(2, 3, 'Guida per principianti', 1, 'Jesse James Garrett', 'Ajax: A New Approach to Web Applications', '2005', 'https://courses.cs.washington.edu/courses/cse490h/07sp/readings/ajax_adaptive_path.pdf', 'article', NULL, '2015-01-05 16:05:30', '2015-01-06 19:10:10'),
(3, 4, '', 1, 'Richard M. Stallman', '2. Language Standards Supported by GCC', '2001', 'http://www.skyfree.org/linux/references/gcc-v3.pdf', 'chapter', NULL, '2015-01-05 22:09:59', '2015-01-06 18:47:55'),
(4, 2, '', 1, 'Dimitri P. Bertsekas', 'Dynamic Programming and Optimal Control', '2005', 'http://athenasc.com/dpbook.html', 'book', NULL, '2015-01-05 23:40:33', '2015-01-06 19:17:46'),
(5, 1, '', 1, 'DB Lange, O Mitsuru', 'Java web services', '1998', 'http://books.google.it/books?hl=it&lr=&id=3qFcdLqtaDIC&oi=fnd&pg=PR7&dq=java&ots=qY57vk3RNT&sig=3TVWCNE6SwDx-JBuPli_bcx0s2Y#v=onepage&q=java&f=false', 'book', NULL, '2015-01-05 23:42:03', '2015-01-06 18:57:50'),
(6, 4, '', 0, 'Richard Stallman', '1 The GNU Project', '2006', 'http://books.google.it/books?hl=it&lr=&id=UJlNAgAAQBAJ&oi=fnd&pg=PA1&dq=stallman&ots=bMqg9VtOkx&sig=ZVY841sGrdVuy5P5sRsFb6cCaDc#v=onepage&q=stallman&f=false', 'chapter', NULL, '2015-01-06 00:03:53', '2015-01-06 18:31:54'),
(9, 5, '', 0, 'Edseger W. Dijkstra', '17. An exercise attributed to R.W. Hamming', '1980', 'http://web.cecs.pdx.edu/~cs410aph/Lectures/Smalltalk%20II/Dijkstra%20on%20Hamming''s%20Problem.pdf', 'chapter', NULL, '2015-01-06 17:29:27', '2015-01-06 17:29:27'),
(10, 5, 'libro a pagamento', 1, 'Edseger W. Dijkstra', 'The Origin of Concurrent Programming', '1965', 'http://link.springer.com/chapter/10.1007/978-1-4757-3472-0_2#page-1', 'book', NULL, '2015-01-06 17:35:09', '2015-01-06 17:35:09'),
(11, 5, '', 1, 'Ghiringhelli, Fabrizio', ' Sistemi di elaborazione delle informazioni', '2014', 'https://www.politesi.polimi.it/handle/10589/94081', 'act', NULL, '2015-01-06 17:40:50', '2015-01-06 17:40:50'),
(12, 5, '', 1, 'Sgariglia, Andrea', 'Chatbot e Interfacce Dialoganti: la nuova frontiera', '2014', 'http://ecum.unicam.it/744/', 'article', NULL, '2015-01-06 17:46:00', '2015-01-06 17:46:00'),
(13, 5, '', 0, 'Piccinini, Enrico', 'Progetto e realizzazione di una  piattaforma scalabile per la  robotica mobile', '2009', 'http://eprints.unife.it/115/', 'book', NULL, '2015-01-06 17:49:50', '2015-01-06 17:49:50'),
(14, 1, 'Titolo Completo: Macchine e utopia: il lavoro, la metropoli, il dominio e la ribellione di fronte alla < Rivoluzione Informatica >', 1, 'Marco Melotti', 'Macchine e utopia: il lavoro, la metropoli, il dominio e la ribellione di fronte alla...', '2011', 'http://books.google.it/books?hl=it&lr=&id=-1j7ALBIHgkC&oi=fnd&pg=PA7&dq=atto+conferenza+informatica&ots=dwmYDfJebQ&sig=zL7pUlpc4Rlq3QzCBGsgzgKcCdM#v=onepage&q&f=false', 'book', NULL, '2015-01-06 18:52:01', '2015-01-06 18:52:01'),
(15, 5, 'algorithm for  C++ programs  with single  level  pointers', 0, 'Hemant D. Pande  and   Barbara G. Ryder', 'Conferenza C++ ', '1994', 'https://www.usenix.org/legacy/publications/library/proceedings/c++94/full_papers/pande.a', 'act', NULL, '2015-01-06 17:56:46', '2015-01-06 17:56:46'),
(16, 1, 'L’informatizzazione è oggi uno dei principali argomenti di discussione in archeologia. A livello disciplinare, stenta però ad affermarsi come un sistema di documentazione necessario ed..', 0, 'Luca Isabella, Federico Salzotti, Marco Valenti', 'L’ESPERIENZA DELL’INSEGNAMENTO DI ARCHEOLOGIA MEDIEVALE A SIENA NEL CAMPO DELL’INFORMATICA APPLICATA', '2000', 'http://www.bibar.unisi.it/sites/www.bibar.unisi.it/files/testi/testisap/23/23-03.pdf', 'article', NULL, '2015-01-06 18:57:22', '2015-01-06 18:57:22'),
(17, 1, '', 1, 'Davide Marocco', 'La robotica evolutiva', '2006', 'http://www.rivisteweb.it/doi/10.1422/21793', 'article', NULL, '2015-01-06 19:00:34', '2015-01-06 19:00:34'),
(22, 5, '', 1, 'Sgariglia, Andrea', 'Chatbot e Interfacce Dialoganti: la nuova frontiera', '2014', 'http://ecum.unicam.it/744/', 'article', 12, '2015-01-06 23:03:56', '2015-01-06 23:03:56');

-- --------------------------------------------------------

--
-- Struttura della tabella `has_tags`
--

DROP TABLE IF EXISTS `has_tags`;
CREATE TABLE IF NOT EXISTS `has_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `document_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_has_tags` (`document_id`),
  KEY `fk_has_tags_1` (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dump dei dati per la tabella `has_tags`
--

INSERT INTO `has_tags` (`id`, `document_id`, `tag_id`) VALUES
(4, 9, 2),
(5, 9, 7),
(6, 10, 7),
(7, 10, 8),
(8, 11, 9),
(9, 11, 10),
(10, 11, 11),
(11, 11, 12),
(12, 12, 13),
(13, 12, 14),
(14, 13, 15),
(15, 14, 16),
(16, 14, 17),
(17, 15, 20),
(18, 16, 12),
(19, 16, 18),
(20, 16, 19),
(21, 17, 15),
(22, 6, 21),
(25, 3, 7),
(26, 3, 22),
(27, 5, 23),
(28, 5, 7),
(29, 4, 7),
(30, 1, 24),
(31, 22, 13),
(32, 22, 14);

-- --------------------------------------------------------

--
-- Struttura della tabella `session_data`
--

DROP TABLE IF EXISTS `session_data`;
CREATE TABLE IF NOT EXISTS `session_data` (
  `session_id` varchar(32) NOT NULL DEFAULT '',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `session_data` blob NOT NULL,
  `session_expire` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `session_data`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dump dei dati per la tabella `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'scienza', '2014-12-23 00:15:03', '2014-12-23 14:54:40'),
(2, 'computer', '2014-12-23 00:15:03', '2014-12-23 14:54:40'),
(3, 'smartphone', '2014-12-23 00:15:03', '2014-12-23 14:54:40'),
(4, 'tablet', '2014-12-23 00:15:03', '2014-12-23 14:54:40'),
(5, 'biologia', '2014-12-23 14:54:40', '2014-12-23 14:54:40'),
(6, 'droni', '2014-12-23 14:57:00', '2014-12-23 14:57:00'),
(7, 'programmazione', '2015-01-06 17:16:20', '2015-01-06 17:16:20'),
(8, 'processi', '2015-01-06 17:33:17', '2015-01-06 17:33:17'),
(9, 'realtaumentata', '2015-01-06 17:39:35', '2015-01-06 17:39:35'),
(10, 'sistemimulti-robot', '2015-01-06 17:39:50', '2015-01-06 17:39:50'),
(11, 'software', '2015-01-06 17:39:55', '2015-01-06 17:39:55'),
(12, 'architettura', '2015-01-06 17:40:02', '2015-01-06 17:40:02'),
(13, 'interfacceintelligenti', '2015-01-06 17:45:27', '2015-01-06 17:45:27'),
(14, 'chatbot', '2015-01-06 17:45:43', '2015-01-06 17:45:43'),
(15, 'robotica', '2015-01-06 17:49:10', '2015-01-06 17:49:10'),
(16, 'macchine', '2015-01-06 18:51:25', '2015-01-06 18:51:25'),
(17, 'lavoro', '2015-01-06 18:51:32', '2015-01-06 18:51:32'),
(18, 'archeologia', '2015-01-06 18:55:39', '2015-01-06 18:55:39'),
(19, 'informaticaapplicata', '2015-01-06 18:55:47', '2015-01-06 18:55:47'),
(20, 'c', '2015-01-06 17:56:09', '2015-01-06 17:56:09'),
(21, 'GNU', '2015-01-06 18:05:51', '2015-01-06 18:05:51'),
(22, 'GCC', '2015-01-06 18:42:00', '2015-01-06 18:42:00'),
(23, 'java', '2015-01-06 18:56:57', '2015-01-06 18:56:57'),
(24, 'analisivisuale', '2015-01-06 19:16:39', '2015-01-06 19:16:39');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(1) NOT NULL COMMENT '0 is MANAGER, 1 is REGISTERED',
  `activated` int(1) NOT NULL COMMENT '0 is NOT ACTIVATED, 1 is ACTIVATED',
  `verification_token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `username`, `password`, `role`, `activated`, `verification_token`, `created_at`, `updated_at`) VALUES
(1, 'Mario', 'Rossi', 'privatedigitallibrary@techrelated.it', 'responsabile', '575f8dca781cb478e0cebe60557a91a1', 0, 1, NULL, '2014-12-23 00:15:03', '2015-01-06 17:00:25'),
(2, 'Sconosciuto', 'Sconosciuto', 'privatedigitallibrary@techrelated.it', 'Sconosciuto', '123', 0, 1, NULL, '2014-12-23 00:15:03', NULL),
(3, 'Nicola', 'Anaclerio', 'nikoan89@gmail.com', 'Nikoan89', '81dc9bdb52d04dc20036dbd8313ed055', 1, 1, '3cPBHmH5yS', '2014-12-23 00:15:03', '2015-01-05 20:22:15'),
(4, 'Vincenzo', 'Lapenta', 'vin.lapenta@gmail.com', 'lapy', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 'eMVhRArHuj', '2014-12-23 00:15:03', '2015-01-06 12:39:19'),
(5, 'Alessandro', 'Bianchi', 'bianchi@techrelated.it', 'a.bianchi', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, NULL, '2014-12-23 00:15:03', '2015-01-06 12:39:19');

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `acts`
--
ALTER TABLE `acts`
  ADD CONSTRAINT `fk_acts` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `fk_articles` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `fk_books` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `chapters`
--
ALTER TABLE `chapters`
  ADD CONSTRAINT `fk_chapters` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `document_modification_tokens`
--
ALTER TABLE `document_modification_tokens`
  ADD CONSTRAINT `fk_edit_user` FOREIGN KEY (`edit_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `fk_documents` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_editof` FOREIGN KEY (`edit_of`) REFERENCES `documents` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Limiti per la tabella `has_tags`
--
ALTER TABLE `has_tags`
  ADD CONSTRAINT `fk_has_tags` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_has_tags_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

# README #

Istruzioni su come usare il repository

# Clonazione Progetto #

### Usi Windows? ###

Step 1. Istallare Git per Windows (http://git-scm.com/download/win)

Step 2. Aprire l'applicazione  "Git Bash"

Step 3. Digitare il seguente comando indicando il proprio nome e cognome:

```
#!shell

git config --global user.name "Nome Cognome"
```

Step 4. Digitare il seguente comando indicando la propria email:

```
#!shell

git config --global user.email "mia-email@example.com"
```

Step 5. Navigare da terminale nella cartella dove si desidera clonare il progetto

Step 4. Digitare il seguente comando sostituendo il vostro username:

```
#!shell

git clone https://username@bitbucket.org/daganivi/pdl.git
```

### Usi Mac o Linux? ###

Step 1. Aprire il terminale e seguire la guida per Windows dal punto 3



# Commits #


### Creazione Commits ###

Dopo aver effettuato cambiamenti ai files si può creare un commit col seguente comando da terminale:


```
#!shell

git commit -m 'Breve descrizione dei cambiamenti'
```

### Push dei commits su Bitbucket ###

Per inviare i cambiamenti al repository digitare il comando:


```
#!shell

git push -u origin master
```
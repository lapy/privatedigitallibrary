<?php

/**
 * PDL - PHP Web Application to manage a collection of documents in a research group
 *
 * @package Private Digital Library
 * @author Vincenzo Lapenta, Nicola Anaclerio
 * @license GNU
 */

// set a constant that holds the project's folder path, like "/var/www/".
// DIRECTORY_SEPARATOR adds a slash to the end of the path
define('ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);
// set a constant that holds the project's "application" folder, like "/var/www/application".
define('APP', ROOT . 'application' . DIRECTORY_SEPARATOR);

// load ActiveRecord class
require APP . 'libs/ActiveRecord.php';

// load ZebraSession class
require APP . 'libs/Zebra_Session.php';

// load PHPmailer class
require APP . 'libs/phpmailer/PHPMailerAutoload.php';

// load application config (error reporting etc.)
require APP . 'config/config.php';

// load application class
require APP . 'core/application.php';
require APP . 'core/controller.php';

// start the application
$app = new Application();

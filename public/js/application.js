/**
*	Funzione che posiziona il footer sempre in fondo alla pagina
*/
function positionFooter() {
	var docHeight = $('#viewport').height() - $("#sticky-footer-push").height();
	if(docHeight < $(window).height()){
		$("#footer").css('position', 'fixed').css('bottom', 0);
	} else {
		$("#footer").css('position', 'static').css('bottom',null);
	}
	// console.log($('#header').width());
	// if($('#header').width() < 1000)
	// {
	// 	$('#documentsmenu').hide();
	// 	$('#loginmenu').hide();
	// 	$('#openmenu').show();
	// }
}

$(window)
         .on('sticky', positionFooter)
        .scroll(positionFooter)
        .resize(positionFooter);


/**
*	Funzione che attiva la paginazione su una pagina index, crea le query di ricerca e aggiorna la lista
*/
function pagination() {
//paginazione
	
	$(document).ready(function() {
		$('.pagination').jqPagination({
			link_string	: '?page={page_number}',
			page_string: 'Pagina {current_page} di {max_page}',
			//max_page: getMaxPage(),
			paged		: function(page) {
				//console.log('(inside jQpag) event page changed');
   				changePage(page);
			}
		});
		$('.elementsPerPage').change(function (){
			resetPage();
		});
		$('.userType').change(function (){
			resetPage();
		});
		$('input.button[value="INVIO"]').click(function (){
			resetPage();
		});
		$('input.search').keyup(function (event){
			if(event.keyCode == 13)
				resetPage();
		});
		$('input.button[value="Reset"]').click(function (event){
			event.preventDefault();
			$('input.search').val('');
			resetPage();
		});
		changePage(1);
	});
}

function fillTable(pageNumber)
{
	pageNumber = pageNumber || 1;
	//console.log('Filling page'+pageNumber);
	var jsonQUERY = jsonURL;
	//jsonQUERY += '' + pageNumber + '/' + getElementsPerPage() + '/' + getQuery() + '?userId=' + getUserId();
	if(pageNumber) {
		jsonQUERY += '' + pageNumber;
		if(getElementsPerPage()) {
			jsonQUERY += '/' + getElementsPerPage();
			if(getQuery())
			{
				jsonQUERY += '/' + getQuery();
			}
		}
	}
	jsonQUERY += '?';
	if(getUserId()) jsonQUERY += 'userId=' + getUserId() + '&';
	if(getTagName()) jsonQUERY += 'tagName=' + getTagName() + '&';
	if(getUserType()) jsonQUERY += 'userType=' + getUserType();

  	$.getJSON(jsonQUERY, function( data ) {
  		var tableHTML = '';
  		//se non ci sono risultati di ricerca
  		if(!('results' in data))
  		{
  			data['results'] = [['<p style="margin: 0 auto; color: red; font-size: 15px;">Nessun elemento trovato</p>']];
  			data['max_page'] = 1;
  		}
  		for(var i = 0; i < data['results'].length; i++) {
  			tableHTML += '<tr>';
  			for(var j = 0; j < data['results'][i].length; j++)
  			{
  				tableHTML += '<td>';
  				tableHTML += data['results'][i][j];  				
  				tableHTML += '</td>';
  			}
  			tableHTML += '</tr>';
  		}
  		if( $('.pagination').jqPagination('option', 'max_page') != data['max_page'] )
  			$('.pagination').jqPagination('option', 'max_page', data['max_page']);

  		var $tab = $(tableHTML);
  		$tab.delay(100).queue(function (next) {
     		$(this).appendTo(tableID);
     		positionFooter();
   			next();
		}).css('opacity', '0').animate({'opacity' : '1'}, { duration: 400 });
	});
}

function clearTable()
{
	$(tableID+' tr:gt(0)').fadeOut(100, function() {
    	$(this).remove();
    });
}

function changePage(pageNumber)
{
	//console.log('Change page'+pageNumber);
   		
	clearTable();
	fillTable(pageNumber);
}

function getQuery()
{
	if(typeof $('input.search').val() !== 'undefined')
		return $('input.search').val().replace(/ /g, '-');
	else
		return '';
}

function getUserId()
{
	if(typeof userId !== 'undefined')
		return userId;
	else
		return '';
}

function getElementsPerPage()
{
	if(typeof $('.elementsPerPage option:selected').val() !== 'undefined')
		return $('.elementsPerPage option:selected').val();
	else
		return '';
}

function getUserType()
{
	if(typeof $('.userType option:selected').val() !== 'undefined')
		return $('.userType option:selected').val()
	else
		return '';
}

function getTagName()
{
	if(typeof tagName !== 'undefined')
		return tagName;
	else
		return '';
}

function getMaxPage()
{
	return Math.ceil(getMaxElements()/getElementsPerPage());
}
function getMaxElements()
{
	pageNumber = 1;
	var jsonQUERY = jsonURL;
	//jsonQUERY += '' + pageNumber + '/' + getElementsPerPage() + '/' + getQuery() + '?userId=' + getUserId();
	if(pageNumber) {
		jsonQUERY += '' + pageNumber;
		if(getElementsPerPage()) {
			jsonQUERY += '/' + getElementsPerPage();
			if(getQuery())
			{
				jsonQUERY += '/' + getQuery();
			}
		}
	}
	jsonQUERY += '?maxelements=true';
	if(getUserId()) jsonQUERY += '&userId=' + getUserId();
	if(getTagName()) jsonQUERY += '&tagName=' + getTagName();
	if(getUserType()) jsonQUERY += '&userType=' + getUserType();
	
	
	var maxelements = null;
	$.ajax({
	  url: jsonQUERY,
	  async: false,
	  dataType: 'json',
	  success: function (json) {
	    maxelements = json;
	  }
	});
	return maxelements;
}

function resetPage()
{
	//console.log('Reset table');
   	var max_page = getMaxPage();

	if( $('.pagination').jqPagination('option', 'max_page') != max_page ) {
		$('.pagination').jqPagination('option', 'current_page', 1);
	} else {
		clearTable();
		fillTable();
	}
}

function fillHomeLatestTable()
{
	$('.widgetTable').each(function(i, val) {
		$.getJSON(jsonURL + val.id, function( data ) {
			var tableHTML = '';
	  		//se non ci sono risultati
	  		if(!('results' in data))
	  		{
	  			data['results'] = [['<p style="margin: 0 auto; color: red; font-size: 15px;">Nessun elemento trovato</p>']];
	  		}
	  		for(var i = 0; i < data['results'].length; i++) {
	  			tableHTML += '<tr>';
	  			for(var j = 0; j < data['results'][i].length; j++)
	  			{
	  				tableHTML += '<td>';
	  				tableHTML += data['results'][i][j];  				
	  				tableHTML += '</td>';
	  			}
	  			tableHTML += '</tr>';
	  		}
	  		var $tab = $(tableHTML);

	  		$(val).append($tab).css('opacity', '0').animate({'opacity' : '1'}, { duration: 500 });
	  		positionFooter();
	  		//console.log('riempita tabella: ' + val.id);
		});
	});
}

//verifica che uno dei valori acquisiti nei form sia presente nel database
$.validator.addMethod("checkInsideDB", function(value, element) {
	var name = element.getAttribute("name");
	var data = {};
	data[name] = value;
	var result = null;
	$.ajax({
		type: "POST",
		url: URL + '/user/ajax/' + element.getAttribute("name"),
		data: data,
		async: false,
		success: function(data) {
			 result = data; 
		},
		dataType: 'json'
	});
	if(result==true)
	{
		//esiste già
		return false;
	}
	else
	{
		//non esiste
		return true;
	}	
});

//verifica che uno dei valori acquisiti nei form sia conforme a quello nel database
$.validator.addMethod("validateData", function(value, element) {
	var name = element.getAttribute("name");
	var data = {};
	data[name] = value;
	data['token'] = verification_token;
	var result = null;
	$.ajax({
		type: "POST",
		url: URL + '/user/ajax/' + element.getAttribute("name"),
		data: data,
		async: false,
		success: function(data) {
			 result = data;
		},
		dataType: 'json'
	});
	if(result==true)
	{
		//esiste già
		return true;
	}
	else
	{
		//non esiste
		return false;
	}	
});

//verifica che la mail non sia già presente nel database
$.validator.addMethod("checkEmail", function(value, element) {
	var name = element.getAttribute("name");
	var data = {};
	data[name] = value;
	var result = null;
	$.ajax({
		type: "POST",
		url: URL + '/user/ajax/' + element.getAttribute("name"),
		data: data,
		async: false,
		success: function(data) {
			 result = data; 
		},
		dataType: 'json'
	});
	if(result==true)
	{
		//esiste già
		return true;
	}
	else
	{
		//non esiste
		return false;
	}	
});

$.validator.addMethod('lessThanEqual', function(value, element, param) {
    if (this.optional(element)) return true;
    var i = parseInt(value);
    var j = parseInt($('[name="' + param + '"]').val());
    return i >= j;
});

/* Italian initialisation for the jQuery UI date picker plugin. */
/* Written by Antonello Pasella (antonello.pasella@gmail.com). */
jQuery(function($){
	$.datepicker.regional['it'] = {
		closeText: 'Chiudi',
		prevText: '&#x3c;Prec',
		nextText: 'Succ&#x3e;',
		currentText: 'Oggi',
		monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',
			'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
		monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu',
			'Lug','Ago','Set','Ott','Nov','Dic'],
		dayNames: ['Domenica','Luned&#236','Marted&#236','Mercoled&#236','Gioved&#236','Venerd&#236','Sabato'],
		dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
		dayNamesMin: ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: '',
		// Personalizzazione
		yearRange: "1900:" + new Date().getFullYear(),
		changeMonth: true,
      	changeYear: true,
      	minDate: new Date (1900, 1 - 1, 1),
      	maxDate: new Date(),
      	showAnim: "slideDown"
    };
	$.datepicker.setDefaults($.datepicker.regional['it']);
});
/*
 * Dropit v1.1.0
 * http://dev7studios.com/dropit
 *
 * Copyright 2012, Dev7studios
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */

(function($) {

    $.fn.dropit = function(method) {

        var methods = {

            init : function(options) {
                this.dropit.settings = $.extend({}, this.dropit.defaults, options);
                return this.each(function() {
                    var $el = $(this),
                         el = this,
                         settings = $.fn.dropit.settings;

                    // Hide initial submenus
                    $el.addClass('dropit')
                    .find('>'+ settings.triggerParentEl +':has('+ settings.submenuEl +')').addClass('dropit-trigger')
                    .find(settings.submenuEl).addClass('dropit-submenu').hide();

                    // Open on click
                    $el.on(settings.action, settings.triggerParentEl +':has('+ settings.submenuEl +') > '+ settings.triggerEl +'', function(){
                        // Close click menu's if clicked again
                        if(settings.action == 'click' && $(this).parents(settings.triggerParentEl).hasClass('dropit-open')){
                            settings.beforeHide.call(this);
                            $(this).parents(settings.triggerParentEl).removeClass('dropit-open').find(settings.submenuEl).slideToggle('fast', function(){});
                            settings.afterHide.call(this);
                            return false;
                        }

                        // Hide open menus
                        settings.beforeHide.call(this);
                        $('.dropit-open').removeClass('dropit-open').find('.dropit-submenu').slideToggle('fast', function(){});
                        settings.afterHide.call(this);

                        // Open this menu
                        settings.beforeShow.call(this);
                        $(this).parents(settings.triggerParentEl).addClass('dropit-open').find(settings.submenuEl).slideToggle('fast', function(){});
                        settings.afterShow.call(this);

                        return false;
                    });

                    // Close if outside click
                    $(document).on('click', function(){
                        settings.beforeHide.call(this);
                        $('.dropit-open').removeClass('dropit-open').find('.dropit-submenu').slideToggle('fast', function(){});
                        settings.afterHide.call(this);
                    });

                    // If hover
                    if(settings.action == 'mouseenter'){
                        $el.on('mouseleave', function(){
                            settings.beforeHide.call(this);
                            $(this).removeClass('dropit-open').find(settings.submenuEl).slideToggle('fast', function(){});
                            settings.afterHide.call(this);
                        });
                    }

                    settings.afterLoad.call(this);
                });
            }

        };

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error( 'Method "' +  method + '" does not exist in dropit plugin!');
        }

    };

    $.fn.dropit.defaults = {
        action: 'click', // The open action for the trigger
        submenuEl: 'ul', // The submenu element
        triggerEl: 'a', // The trigger element
        triggerParentEl: 'li', // The trigger parent element
        afterLoad: function(){}, // Triggers when plugin has loaded
        beforeShow: function(){}, // Triggers before submenu is shown
        afterShow: function(){}, // Triggers after submenu is shown
        beforeHide: function(){}, // Triggers before submenu is hidden
        afterHide: function(){} // Triggers before submenu is hidden
    };

    $.fn.dropit.settings = {};

})(jQuery);

//aggiunge gli anni alla select del campo #year
function yearSelect(selectedYear) {
	selectedYear = selectedYear || 0;

	for (i = new Date().getFullYear(); i >= 1900; i--)
	{
		if (selectedYear == i)
			$('#year').append($('<option />').val(i).html(i).attr('selected', true));
		else
	    	$('#year').append($('<option />').val(i).html(i));
	}
}

//funzione per finestra di dialogo eliminazione utente
function hookDelete(event, link) {
	event.preventDefault();
	var theHREF = $(link).attr("href");
    var theREL = $(link).attr("rel");
    var theMESSAGE = 'Sei sicuro di voler eliminare questo utente?';
    var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
    
    // set windows content
    $('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');
    
    $("#dialog").dialog('option', 'buttons', {
        "Conferma" : function() {
            window.location.href = theHREF;
        },
        "Annulla" : function() {
            $(this).dialog("close");
        }
    });
    $("#dialog").dialog("open");
}

$(document).ready(function() {
	//attivo il menu dropdown
    $('#controlpanelmenu').dropit();
    $('#documentmenu').dropit();
    $('#openmenu').dropit();
    $( ".radiogroup" ).buttonset();
    //posiziono il footer
	positionFooter();
	//nascondo i warning vuoti
	if($('.warning').text() == '')
		$('.warning').remove();
	//aziono il pulsante login
	$('.loginlink').click(function(){
		$('#login').slideToggle('fast', function(){});
	});
});